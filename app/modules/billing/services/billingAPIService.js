'use strict';

/**
 * @ngdoc service
 * @name products.Services.Rest
 * @description Rest Factory
 */
    angular
        .module('billing')
        .factory('RestBilling', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    getCustomersInvoiceList: function (customer_id,token) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCustomersInvoiceList';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token};
                        return $http.post(url, rest_data,config);
                    },

                    getInvoiceDetail: function (customer_id,token,invoice_id) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getInvoiceDetail';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token,"invoice_id":invoice_id};
                        return $http.post(url, rest_data,config);
                    },
					
                    getCustomerPaymentsList: function (customer_id,token) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCustomerPaymentsList';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token};
                        return $http.post(url, rest_data,config);
                    },

                     
                }
        }]);