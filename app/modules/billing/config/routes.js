'use strict';

/**
 * @ngdoc object
 * @name billing.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the billing module
 */
angular
        .module('billing')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name billing.config.route
                 * @eventOf billing.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                
                         .state('billings', {
                            url: '/billings',
                            templateUrl: 'modules/billing/views/invoice_list.html',
                            controller: 'BillingsController'
                        })
                        .state('payment_history', {
                           url: '/payment_history',
                           templateUrl: 'modules/billing/views/payment_history.html',
                           controller: 'PaymentHistoryController'
                       })
                       
                        .state('invoice_detail', {
                           url: '/invoice_detail/:invoice_id?',
                           templateUrl: 'modules/billing/views/invoice_detail.html',
                           controller: 'InvoiceDetailController'
                       })
                       
                        .state('subscription_invoice', {
                           url: '/subscription_invoice',
                           templateUrl: 'modules/billing/views/subscription_invoice_detail.html',
                           controller: 'SubscriptionInvoiceDetailController'
                       })
                       


            }
        ]);
