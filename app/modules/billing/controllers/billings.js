'use strict';

/**
 * @ngdoc object
 * @name billing.Controllers.HomeController
 * @description Billing/Invoice controller
 * @requires ng.$scope
 */
angular
    .module('billing')
    .controller('BillingsController', ['$scope','$rootScope','$window','RestBilling','$state',
        function($scope,$rootScope,$window,RestBilling,$state) {
            
          $rootScope.page_heading = "Invoices";  
          

        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
        $scope.customer_id =   $scope.session.customer_id;
        $scope.token =   $scope.session.token;
         
        
        /* Get the List of Orders Place by Customer
         * Date : 30/05/2017
         * 
         */
        $scope.getMyInvoiceList = function(customer_id,token) {

            $scope.showLoader(false);

            RestBilling.getCustomersInvoiceList(customer_id,token).success(function(response)
            {

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.invoice_list = response["result"];
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                   // alert(response["message"]);
                    $scope.no_list = true; 
                    $scope.showLoader(true);
                    
                    $scope.showAlertMessage("Alert",response["message"]); 
                    if (response["status"] === 'invalid_token') {
                        $rootScope.invalidToken();
                    }
                    
                    
                }

            }).error(function(response) {
                //alert("Error in connection.");
                $scope.showLoader(true);	
                //alert(response.message);   
               //bootbox.dialog({title: 'Alert', message: response.message }); 
               $scope.showAlertMessage('Alert',response.message);
                $state.go("home", {});
            });

        };

        $scope.getMyInvoiceList($scope.customer_id,$scope.token);
            
          
        }
    ])


    .controller('InvoiceDetailController', ['$scope','$rootScope','$window','RestBilling','$state','Constant',
        function($scope,$rootScope,$window,RestBilling,$state,Constant) {
            
			$rootScope.page_heading = "Invoice Detail";  


			$scope.session = angular.fromJson($window.localStorage.getItem("user_session"));

			//console.log($scope.session);
			$scope.customer_id =   $scope.session.customer_id;
			$scope.token =   $scope.session.token;
			$scope.invoice_id = $state.params.invoice_id;

			/* 
			* Get the List of Orders Place by Customer
			* Date : 30/05/2017
			* 
			*/
			$scope.getInvoiceDetail = function(customer_id,token,invoice_id) {

				$scope.showLoader(false);

				RestBilling.getInvoiceDetail(customer_id,token,invoice_id).success(function(response)
				{

					if (response["status"] == "success")
					{

						$scope.totalCount = response["totalCount"];
					 
						
						$scope.invoice_detail = response["result"]["Invoice_info"];
						$scope.invoice_status = $scope.invoice_detail['invoice_status'];
						
						$scope.product_list = response["result"]["product_info"];
						
						$scope.showLoader(true);
						$scope.no_list = false; 
					}
					else
					{
					   // alert(response["message"]);
						$scope.no_list = true; 
						$scope.showLoader(true);
                                                $scope.showAlertMessage("Alert",response["message"]); 
                                                if (response["status"] === 'invalid_token') {
                                                    $rootScope.invalidToken();
                                                }
                                                
                                                
					}

				}).error(function(response) {              
					$scope.showLoader(true);	
					//alert(response.message);  
					//bootbox.dialog({title: 'Alert', message: response.message });
                    $scope.showAlertMessage('Alert',response.message);
					$state.go("home", {});
				});

			};
			
			
			/* 
			* Make Invoice Payment
			* fun :
			* Date : 30/05/2017
			* 
			*/


				$scope.makeInvoicePayment = function(invoice_id,invoice_amount) 
				{


					var session = angular.fromJson($window.localStorage.getItem("user_session"));


					if (session == "" || session == null)
					{
						//alert("Please login to make invoice Payment.");
						//bootbox.dialog({title: 'Alert', message: "Please login to make invoice Payment." }); 
                        $scope.showAlertMessage('Alert',"Please login to make invoice Payment.");
						$scope.showLoader(true);
						$state.go("login");

					}
					else
					{

						$scope.customer_id = session.customer_id;
						$scope.token = session.token;

						
						$scope.invoice_id =invoice_id;
						$scope.invoice_amount =invoice_amount;
						
						var tid = new Date().getTime();

						var ccavenue_url = Constant.CCAVENUE_URL + "/ccavRequestHandler.php?invoice_id=" + $scope.invoice_id + "&customer_id=" + $scope.customer_id + "&invoice_amount=" + $scope.invoice_amount + "&tid=" + tid;

						
						if (Constant.DEVICE_PLATFORM === "web")
						{
							$window.open(ccavenue_url);
							$scope.showLoader(false);
							setTimeout(function() {
								$state.go("payment_status", { 'invoice_id': $scope.invoice_id});
							}, 6500);
						}
						else if (Constant.DEVICE_PLATFORM === "Android")
						{
                                                       // var options = {location: 'yes', EnableViewPortScale: 'yes', toolbar: 'yes'}; 
                                                        
                                                        var ref = cordova.InAppBrowser.open(ccavenue_url, '_blank', 'location=no,EnableViewPortScale=yes,toolbar=no');
                                                        $scope.showLoader(true);

							// attach listener to loadstart
							ref.addEventListener('loadstart', function(event) {
								var urlSuccessPage = Constant.CCAVENUE_RESPONSE_URL;
								if (event.url == urlSuccessPage) {
                                            
                                                                    setTimeout(function() {
                                                                       ref.close();
                                                                       $state.go("payment_status", { 'invoice_id': $scope.invoice_id});
                                                                    }, 12000);
								}
							});
						}

					}


				};

			

			$scope.getInvoiceDetail($scope.customer_id,$scope.token,$scope.invoice_id);

          
        }
    ])
	
	
    .controller('PaymentHistoryController', ['$scope','$rootScope','$window','RestBilling','$state',
        function($scope,$rootScope,$window,RestBilling,$state) {
            
          $rootScope.page_heading = "Payment History";  
          

        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
         
        
        /* Get the List of Orders Place by Customer
         * Date : 30/05/2017
         * 
         */
        $scope.getPaymentHistoryList = function() {

            $scope.showLoader(false);
			
			$scope.customer_id =   $scope.session.customer_id;
			$scope.token =   $scope.session.token;

            RestBilling.getCustomerPaymentsList($scope.customer_id,$scope.token).success(function(response)
            {

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.payment_list = response["result"];
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                   // alert(response["message"]);
                    $scope.no_list = true; 
                    $scope.showLoader(true);
                    
                    $scope.showAlertMessage("Alert",response["message"]); 
                    if (response["status"] === 'invalid_token') {
                        $rootScope.invalidToken();
                    }
                    
                }

            }).error(function(response) {
                //alert("Error in connection.");
                $scope.showLoader(true);	
                //alert(response.message);   
                //bootbox.dialog({title: 'Alert', message: response.message }); 
                $scope.showAlertMessage('Alert',response.message);
                $state.go("home", {});
            });

        };

        $scope.getPaymentHistoryList($scope.customer_id,$scope.token);
            
          
        }
    ])
	

	
    .controller('SubscriptionInvoiceDetailController', ['$scope','$rootScope','$window','RestBilling','$state','Constant',
        function($scope,$rootScope,$window,RestBilling,$state,Constant) {
            
			$rootScope.page_heading = "Invoice Detail";  


			$scope.session = angular.fromJson($window.localStorage.getItem("user_session"));

			//console.log($scope.session);

		


          
        }
    ])
    
    .controller('PaymentStatusController', ['$scope', '$rootScope', 'RestCart', '$window', 'Constant', '$state',
        function($scope, $rootScope, RestCart, $window, Constant, $state) {

            $rootScope.page_heading = "Payment Status";
            $rootScope.products_cart = [];
            $rootScope.cart_count = 0;

            $scope.invoice_id = $state.params.invoice_id;
            var session = angular.fromJson($window.localStorage.getItem("user_session"));

            $scope.customer_id = session.customer_id;
            $scope.token = session.token;

            $scope.showLoader(false);

            $scope.cart_object = {'customer_id': $scope.customer_id, 'token': $scope.token,'invoice_id': $scope.invoice_id}
            $rootScope.products_cart = [];
            // Check Order Status 
            RestCart.checkOrderPaymentStatus($scope.cart_object).success(function(response)
            {                
                $scope.status = response["status"];

                if($scope.status == "success")
                {

                    $scope.showLoader(true);
                    $scope.payment_status = response["result"]["payment_status"];
                   // alert($scope.payment_status);
                    $scope.message_display = response["result"]["message_display"];
                }
                else
                {
                    $scope.showAlertMessage("Alert",$scope.message); 
                    if ($scope.status === 'invalid_token') {
                        $rootScope.invalidToken();
                    }

                }

            }).error(function(response) {                   
                //alert("Error in connection.");
                 //bootbox.dialog({title: 'Alert', message: "Error in connection."}); 
                 $scope.showAlertMessage('Alert',"Error in connection.");   
                $scope.showLoader(true);	
            });




        }
    ]);
    
	