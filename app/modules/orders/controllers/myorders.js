'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('MyordersController', ['$scope','$rootScope','$window','RestOrder','$state',
        function($scope,$rootScope,$window,RestOrder,$state) {
            
        $rootScope.page_heading = "My Orders";   
        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
        $scope.customer_id =   $scope.session.customer_id;
        $scope.token =   $scope.session.token;
         
        
        /* Get the List of Orders Place by Customer
         * Date : 30/05/2017
         * 
         */
        $scope.getMyOrderList = function(customer_id,token) {

            $scope.showLoader(false);

            RestOrder.getCustomersOrderList(customer_id,token).success(function(response)
            {

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.order_list = response["result"];
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                   // alert(response["message"]);
                    $scope.no_list = true; 
                    $scope.showLoader(true);
                    
                    $scope.showAlertMessage("Alert",response["message"]); 
                    if (response["status"] === 'invalid_token') {
                        $rootScope.invalidToken();
                    }
                    
                    
                }

            }).error(function(response) {                
                $scope.showLoader(true);
                //alert(response.message);  
               // bootbox.dialog({title: 'Alert', message: response.message});  
               $scope.showAlertMessage('Alert',response.message);
                $state.go("home", {});
            });

        };

        $scope.getMyOrderList($scope.customer_id,$scope.token);
         
         
         
         
        }
    ])

    .controller('MyordersDetailController', ['$scope','$rootScope','$window','RestOrder','$state',
        function($scope,$rootScope,$window,RestOrder,$state) {
            
        $rootScope.page_heading = "Order Detail";   
        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
        $scope.customer_id =   $scope.session.customer_id;
        $scope.token =   $scope.session.token;
        $scope.order_id = $state.params.order_id;//$rootScope.aform_id; 
        
        /* Get Order Details
         * Date : 08/06/2017
         * 
         */
        $scope.getOrderDetail = function(customer_id,token,order_id) {

            $scope.showLoader(false);

            RestOrder.getOrderDetail(customer_id,token,order_id).success(function(response)
            {

                if (response["status"] == "success")
                {

                    
                    $scope.order_detail = response["result"]["Order_info"];
                    $scope.product_list = response["result"]["product_info"];
                    
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                   // alert(response["message"]);
                    $scope.no_list = true; 
                    $scope.showLoader(true);
                    
                    $scope.showAlertMessage("Alert",response["message"]); 
                    if (response["status"] === 'invalid_token') {
                        $rootScope.invalidToken();
                    }
                    
                }

            }).error(function(response) {
               // alert("Error in connection.");
              // bootbox.dialog({title: 'Alert',message:"Error in connection"});
              $scope.showAlertMessage('Alert',"Error in connection."); 
                $scope.showLoader(true);	
            });

        };

        $scope.getOrderDetail($scope.customer_id,$scope.token,$scope.order_id);
         
          
         
        }
    ]);

