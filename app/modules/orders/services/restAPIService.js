'use strict';

/**
 * @ngdoc service
 * @name products.Services.Rest
 * @description Rest Factory
 */
    angular
        .module('orders')
        .factory('RestOrder', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    getCustomersOrderList: function (customer_id,token) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getMyOrderList';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token};
                        return $http.post(url, rest_data,config);
                    },

                    getOrderDetail: function (customer_id,token,order_id) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getOrderDetail';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token,"order_id":order_id};
                        return $http.post(url, rest_data,config);
                    },
                    
                }
        }]);