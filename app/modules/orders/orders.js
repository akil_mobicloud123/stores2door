'use strict';

/**
 * @ngdoc overview
 * @name orders
 * @description The angular services, filters, directives, filters within the orders module are accessible throughout the angular app like any other provider within the app, but these providers do not necessarily belong to any particular module, hence their placement would be here.
 */
ApplicationConfiguration.registerModule('orders');
