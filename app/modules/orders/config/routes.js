'use strict';

/**
 * @ngdoc object
 * @name orders.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the orders module
 */
angular
        .module('orders')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name orders.config.route
                 * @eventOf orders.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                

                         .state('myorders', {
                            url: '/myorders',
                            templateUrl: 'modules/orders/views/myorders.html',
                            controller: 'MyordersController'
                        })
                       
                         .state('order_detail', {
                            url: '/order_detail/:order_id?',
                            templateUrl: 'modules/orders/views/order_detail.html',
                            controller: 'MyordersDetailController'
                        })
                       
                       


            }
        ]);
