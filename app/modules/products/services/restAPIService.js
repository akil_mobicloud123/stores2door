'use strict';

/**
 * @ngdoc service
 * @name products.Services.Rest
 * @description Rest Factory
 */
    angular
        .module('products')
        .factory('RestProduct', ['$http','$window','Constant', function ($http,$window,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    getCategoryList: function () {
                        // Set the Content-Type 
                   
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCategoryList';
                        
                        var config = {};
                        var rest_data = {};
                        return $http.post(url, rest_data,config);
                    },

                    getSubCategoryList: function (product_category) {
                        // Set the Content-Type 
                       
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getSubCategoryList';
                        
                        var config = {};
                        var rest_data = {"category_id" : product_category};
                        return $http.post(url, rest_data,config);
                    },

                    getSubSubCategoryList: function (product_category,product_sub_category) {
                        // Set the Content-Type 
                       
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getSubSubCategoryList';
                        
                        var config = {};
                        var rest_data = {"category_id" : product_category,
                        "subcategory_id" : product_sub_category    
                    };
                        return $http.post(url, rest_data,config);
                    },


                    getProducList: function (product_category,product_sub_category,product_subtosub_category) {

                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getProductList';
                        
                        var config = {};
                        var rest_data = {"category_id" : product_category,
                        "subcategory_id" : product_sub_category,
                        "subsubcategory_id" : product_subtosub_category  
                                   };
                        return $http.post(url, rest_data,config);
                    },


                    getMyProductList: function () {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getMyProductList';
                        var session = angular.fromJson($window.localStorage.getItem("user_session"));
                        var config = {};
                        var rest_data = {"customer_id":session.customer_id};
                        return $http.post(url, rest_data,config);
                    },



                    getProductDetail: function (product_id) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getProductDetail';
                        
                        var config = {};
                        var rest_data = {"product_id":product_id};
                        return $http.post(url, rest_data,config);
                    },
                    getPayProductList: function (product_category) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getPayProductList';
                        
                        var config = {};
                        var rest_data = {"category_id" : product_category};
                        return $http.post(url, rest_data,config);
                    },



                    updateMyProductList: function (productId,checkMylist) {

                        debugger
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'updateMyProductList';

                        var session = angular.fromJson($window.localStorage.getItem("user_session"));
                        
                        var config = {};
                        var rest_data = {"product_id" : productId,
                              "check_mylist":checkMylist,
                              "customer_id":session.customer_id              
                    };
                        return $http.post(url, rest_data,config);
                    },

					
                    
                }
        }]);