'use strict';

/**
 * @ngdoc object
 * @name core.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the core module
 */
angular
        .module('products')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name products.config.route
                 * @eventOf products.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                
			.state('products', {
                            url: '/products',
                            templateUrl: 'modules/products/views/products.html',
                            controller: 'ProductsController'
                        })
                        .state('productsdetails', {
                            url: '/productsdetails/:product_id?',
                            templateUrl: 'modules/products/views/productsdetails.html',
                            controller: 'ProductsDetailsController'
                        })
			.state('pay_products', {
                            url: '/pay_products',
                            templateUrl: 'modules/products/views/pay_products.html',
                            controller: 'PayProductsController'
                        })

                        .state('products_cat1', {
                            url: '/products_cat1',
                            templateUrl: 'modules/products/views/products_cat1.html',
                            controller: 'ProductsCat1Controller'
                        })
                        
                        .state('products_cat2', {
                            url: '/products_cat2',
                            templateUrl: 'modules/products/views/products_cat2.html',
                            controller: 'ProductsCat2Controller'
                        })
                        
                        

                        .state('Beauty_products', {
                            url: '/Beauty_products',
                            templateUrl: 'modules/products/views/Beauty_products.html',
                            controller: 'BeautyProductsController'
                        })

                        .state('Beverage_products', {
                            url: '/Beverage_products',
                            templateUrl: 'modules/products/views/Beverage_products.html',
                            controller: 'BeverageProductsController'
                        })


                        .state('Cleaning_products', {
                            url: '/Cleaning_products',
                            templateUrl: 'modules/products/views/Cleaning_products.html',
                            controller: 'CleaningProductsController'
                        })


                        .state('meat_products', {
                            url: '/meat_products',
                            templateUrl: 'modules/products/views/meat_products.html',
                            controller: 'MeatProductsController'
                        })

                        .state('products_Item', {
                            url: '/products_Item',
                            templateUrl: 'modules/products/views/products_Item.html',
                            controller: 'ProductsItemController'
                        })
 
                        .state('beauty_cat1', {
                            url: '/beauty_cat1',
                            templateUrl: 'modules/products/views/beauty_cat1.html',
                            controller: 'BeautyCat1Controller'
                        })

                        .state('beauty_cat2', {
                            url: '/beauty_cat2',
                            templateUrl: 'modules/products/views/beauty_cat2.html',
                            controller: 'BeautyCat2Controller'
                        })


                        .state('beverage_cat1', {
                            url: '/beverage_cat1',
                            templateUrl: 'modules/products/views/beverage_cat1.html',
                            controller: 'BeverageCat1Controller'
                        })

                        .state('beverage_cat2', {
                            url: '/beverage_cat2',
                            templateUrl: 'modules/products/views/beverage_cat2.html',
                            controller: 'BeverageCat2Controller'
                        })

                        .state('cleaning_cat1', {
                            url: '/cleaning_cat1',
                            templateUrl: 'modules/products/views/cleaning_cat1.html',
                            controller: 'CleaningCat1Controller'
                        })


                        .state('cleaning_cat2', {
                            url: '/cleaning_cat2',
                            templateUrl: 'modules/products/views/cleaning_cat2.html',
                            controller: 'CleaningCat2Controller'
                        })

                        .state('meat_cat1', {
                            url: '/meat_cat1',
                            templateUrl: 'modules/products/views/meat_cat1.html',
                            controller: 'MeatCat1Controller'
                        })



                        .state('meat_cat2', {
                            url: '/meat_cat2',
                            templateUrl: 'modules/products/views/meat_cat2.html',
                            controller: 'MeatCat2Controller'
                        })


                        .state('fruits_products', {
                            url: '/fruits_products',
                            templateUrl: 'modules/products/views/fruits_products.html',
                            controller: 'FruitsProductsController'
                        })



                        .state('Grumnats_products', {
                            url: '/Grumnats_products',
                            templateUrl: 'modules/products/views/Grumnats_products.html',
                            controller: 'GrumnatsProductsController'
                        })


                        .state('snacks_products', {
                            url: '/snacks_products',
                            templateUrl: 'modules/products/views/snacks_products.html',
                            controller: 'SnacksProductsController'
                        })


                        .state('mylist', {
                            url: '/mylist',
                            templateUrl: 'modules/products/views/mylist.html',
                            controller: 'MyListController'
                        })


                        .state('sub_category', {
                            url: '/sub_category',
                            templateUrl: 'modules/products/views/sub_category.html',
                            controller: 'SubCategoryController'
                        })


                        .state('under_sub_category', {
                            url: '/under_sub_category',
                            templateUrl: 'modules/products/views/under_sub_category.html',
                            controller: 'UnderSubCategoryController'
                        })



            }
        ]);
