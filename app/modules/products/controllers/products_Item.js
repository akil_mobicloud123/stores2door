'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('products')
    .controller('ProductsItemController', ['$scope','$rootScope','RestProduct','$state',
        function($scope,$rootScope,RestProduct,$state) {
           
        $rootScope.page_heading = "Our Products";  
            
        /* Get The Product Listing Page 
         * Date : 30/05/2017
         * 
         */
        
        $scope.productList = function() {

            $scope.showLoader(false);
            
            // Display Milk Products Only
            $scope.product_category =1;
            
            RestProduct.getProducList($scope.product_category).success(function(response)
            {
                
                $scope.status = response["status"];

                //alert($scope.status);

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_list = response["result"];
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                    // $state.go("home");
                    $scope.no_list = true; 
                     $scope.showLoader(true);
                     
                }

            }).error(function(response) {
                //alert("Error in connection.");  
                //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
                $scope.showAlertMessage('Alert',"Error in connection.");    
                $scope.showLoader(true);
                $state.go("home", {});
                
            });

        };

        $scope.productList();
            
       
        
        }
        
        
        
       
    ])
    
    .controller('ProductsDetailsController', ['$scope','$rootScope','RestProduct','$state',
        function($scope,$rootScope,RestProduct,$state) {
           
        $rootScope.page_heading = "Product Detail";  
        $scope.product_id = $state.params.product_id;//$rootScope.aform_id;;

            
        /* Get the detail of selected Product 
         * Date : 30/05/2017
         * 
         */
       
            $scope.showLoader(false);

            RestProduct.getProductDetail($scope.product_id).success(function(response)
            {
                
                $scope.status = response["status"];

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_detail = response["result"][0];
                    //console.log($scope.product_detail);
                    $scope.showLoader(true);
                }
                else
                {
                    // $state.go("home");
                     $scope.showLoader(true);
                }

            }).error(function(response) {
               // alert("Error in connection.");
                //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
                $scope.showAlertMessage('Alert',"Error in connection.");
                $scope.showLoader(true);	
            });
       
         
         
        }
    ]);
