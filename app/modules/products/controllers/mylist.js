'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('MyListController', ['$scope','$rootScope','RestProduct','$state',
    function($scope,$rootScope,RestProduct,$state) {
            
            $rootScope.page_heading = "My Product List"; 
            $scope.status_value=true;
            $scope.productList = function() {
                debugger
        $scope.showLoader(false);
        
        // Display Milk Products Only
        // $scope.product_category =$rootScope.catogory_id;
        // $scope.product_category_title=$rootScope.catogory_title;
        RestProduct.getMyProductList().success(function(response)
        {
            debugger
            $scope.status = response["status"];

            //alert($scope.status);

            if (response["status"] == "success")
            {

                $scope.totalCount = response["totalCount"];
                $scope.product_list = response["result"];
                $scope.showLoader(true);
                $scope.no_list = false; 
            }
            else
            {
                // $state.go("home");
                $scope.no_list = true; 
                 $scope.showLoader(true);
                 
            }

        }).error(function(response) {
            //alert("Error in connection.");  
            //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
            $scope.showAlertMessage('Alert',"Error in connection.");    
            $scope.showLoader(true);
            $state.go("home", {});
            
        });

    };

    $scope.productList();

    $scope.removeToMylist = function(productId,statusList) {
        debugger


// Display Milk Products Only

$scope.product_id=productId;
$scope.checkListStatus=statusList.toString();
RestProduct.updateMyProductList($scope.product_id,$scope.checkListStatus).success(function(response)
{
    debugger
    $scope.productList();
    // $scope.status = response["status"];

    // //alert($scope.status);

    // if (response["status"] == "success")
    // {

    //     $scope.totalCount = response["totalCount"];
    //     $scope.product_list = response["result"];
    //     $scope.showLoader(true);
    //     $scope.no_list = false; 
    // }
    // else
    // {
    //     // $state.go("home");
    //     $scope.no_list = true; 
    //      $scope.showLoader(true);
         
    // }

}).error(function(response) {
    //alert("Error in connection.");  
    //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
    $scope.showAlertMessage('Alert',"Error in connection.");    
    $scope.showLoader(true);
    $state.go("home", {});
    
});

};

        }
    ]);
