'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('products')
    .controller('BeautyProductsController', ['$scope','$rootScope','RestProduct','$window','$location','$state',
        function($scope,$rootScope,RestProduct,$window,$location,$state) {
           
        $rootScope.page_heading = "Products";  
            
        /* Get The Product Listing Page 
         * Date : 30/05/2017
         * 
         */

        $scope.showFilter=function(){
            
            $("#filter_div").removeClass('filter-div-hide');
            $("#filter_div").addClass('filter-div-show');
         }
    
  
    
         $scope.hideFilter=function(){
             
            $("#filter_div").removeClass('filter-div-show');
            $("#filter_div").addClass('filter-div-hide');
         }

         $scope.showSort=function(){
            
            $("#sort_div").removeClass('sort-div-hide');
            $("#sort_div").addClass('sort-div-show');
         }
    
  
    
         $scope.hideSort=function(){
             
            $("#sort_div").removeClass('sort-div-show');
            $("#sort_div").addClass('sort-div-hide');
         }
        
        $scope.productList = function() {
               
            $scope.showLoader(false);
            
            // Display Milk Products Only
            // $scope.product_category =$rootScope.catogory_id;
            // $scope.product_sub_category =$rootScope.catogory_sub_id;
            // $scope.product_category_title=$rootScope.catogory_title;
            
            $scope.product_category=$window.localStorage.getItem("catogory_id");
            $scope.product_sub_category=$window.localStorage.getItem("catogory_sub_id");
            $scope.product_subtosub_category=$window.localStorage.getItem("catogory_subtosub_id");
            $scope.product_category_title=$window.localStorage.getItem("catogory_sub_title");
            $rootScope.page_heading = $scope.product_category_title; 
            RestProduct.getProducList($scope.product_category,$scope.product_sub_category,$scope.product_subtosub_category).success(function(response)
            {
                
                $scope.status = response["status"];

                //alert($scope.status);

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_list = response["result"];
                    $scope.showLoader(true);
                    $scope.no_list = false; 
                }
                else
                {
                    // $state.go("home");
                    $scope.no_list = true; 
                     $scope.showLoader(true);
                     
                }

            }).error(function(response) {
                //alert("Error in connection.");  
                //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
                $scope.showAlertMessage('Alert',"Error in connection.");    
                $scope.showLoader(true);
                $state.go("home", {});
                
            });

        };

        $scope.productList();

        $scope.addToMylist = function(productId,statusList) {
        

    
    // Display Milk Products Only
    
    $scope.product_id=productId;
    $scope.checkListStatus=statusList.toString();
    RestProduct.updateMyProductList($scope.product_id,$scope.checkListStatus).success(function(response)
    {
   
        // $scope.status = response["status"];
               window.document.getElementById("").innerHTML="Added";
        // //alert($scope.status);

        // if (response["status"] == "success")
        // {

        //     $scope.totalCount = response["totalCount"];
        //     $scope.product_list = response["result"];
        //     $scope.showLoader(true);
        //     $scope.no_list = false; 
        // }
        // else
        // {
        //     // $state.go("home");
        //     $scope.no_list = true; 
        //      $scope.showLoader(true);
             
        // }

    }).error(function(response) {
        //alert("Error in connection.");  
        //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
        $scope.showAlertMessage('Alert',"Error in connection.");    
        $scope.showLoader(true);
        $state.go("home", {});
        
    });

};
            
       
        
        }
        
        
        
       
    ])
    
    .controller('ProductsDetailsController', ['$scope','$rootScope','RestProduct','$state',
        function($scope,$rootScope,RestProduct,$state) {
           
        $rootScope.page_heading = "Product Detail";  
        $scope.product_id = $state.params.product_id;//$rootScope.aform_id;;

            
        /* Get the detail of selected Product 
         * Date : 30/05/2017
         * 
         */
       
            $scope.showLoader(false);

            RestProduct.getProductDetail($scope.product_id).success(function(response)
            {
                
                $scope.status = response["status"];

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_detail = response["result"][0];
                    //console.log($scope.product_detail);
                    $scope.showLoader(true);
                }
                else
                {
                    // $state.go("home");
                     $scope.showLoader(true);
                }

            }).error(function(response) {
               // alert("Error in connection.");
                //bootbox.dialog({title: 'Alert',message:"Error in connection"}); 
                $scope.showAlertMessage('Alert',"Error in connection.");
                $scope.showLoader(true);	
            });
       
         
         
        }
    ]);
