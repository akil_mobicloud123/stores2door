'use strict';

/**
 * @ngdoc object
 * @name subscription.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the subscription module
 */
angular
        .module('subscription')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name subscription.config.route
                 * @eventOf subscription.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                
                        .state('subscription', {
                            url: '/subscription',
                            templateUrl: 'modules/subscription/views/subscription.html',
                            controller: 'SubscriptionController'
                        })
                       
                       .state('holdsubscription', {
                           url: '/holdsubscription',
                           templateUrl: 'modules/subscription/views/holdsubscription.html',
                           controller: 'HoldsubscriptionController'
                       })
                       
						.state('calenderView', {
							url: '/calenderView',
							templateUrl: 'modules/subscription/views/calenderView.html',
							controller: 'CalenderviewController'
						})

                        .state('my_subscription_detail', {
                            url: '/my_subscription_detail',
                            templateUrl: 'modules/subscription/views/my_subscription_detail.html',
                            controller: 'MySubscriptionDetailController'
                        })
                        .state('unsubscribe_detail', {
                            url: '/unsubscribe_detail',
                            templateUrl: 'modules/subscription/views/unsubscription_detail.html',
                            controller: 'UnSubscriptionDetailController'
                        });
						
            }
        ]);
