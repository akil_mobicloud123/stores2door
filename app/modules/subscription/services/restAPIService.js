'use strict';

/**
 * @ngdoc service
 * @name core.Services.Rest
 * @description Rest Factory
 */
angular
        .module('core')
        .factory('restAPISubscription', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    /**
                     * customer Subscription Registration
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    customerSubscriptionRegistation: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'customerSubscriptionRegistation';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },
                    /**
                     * Hold subscription
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    holdSubscription: function (hold_subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'holdSubscription';
                        var config = {};
                        var rest_data = hold_subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },
                    /**
                     * Get Monthly Subscription details
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    getMonthlySubscriptionDetail: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getMonthlySubscriptionDetail';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },
                    /**
                     * Get Monthly Subscription details
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    mysubscriptionDetail: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'mysubscriptionDetail';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },
                    /**
                     * Get Monthly Subscription details
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    viewProductSubscriptionDetails: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getMonthlySubscriptionDetail';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },
                    /**
                     * Update Product Quantity For Day i Subscription
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    updateSubscriptionProductQuantityForDay: function (edit_subscription_object) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'updateSubscriptionProductQuantityForDay';
                        var config = {};
                        var rest_data = edit_subscription_object;                       
                        return $http.post(url, rest_data,config);
                    },

                    /**
                     * Unsubscribe Product
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    unsubscribeProduct: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'unsubscribe_product';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },

                    /**
                     * UnHold Product Subscription
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    unHoldProductSubscription: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'unHoldProductSubscription';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },

                    /**
                     * Get Product Subscription details
                     * @param {type} subscription_data
                     * @returns {unresolved}
                     */
                    getProductSubscriptionDetails: function (subscription_data) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getProductSubscriptionDetails';
                        var config = {};
                        var rest_data = subscription_data;                       
                        return $http.post(url, rest_data,config);
                    },

                    /**
                     * Modify the Product Quantity Till End Of Month
                     * @param {type} edit_subscription_object
                     * @returns {unresolved}
                     */
                    modifySubscriptionProductQuantityForMonth: function (edit_subscription_object) {                        
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'modifySubscriptionProductQuantityForMonth';
                        var config = {};
                        var rest_data = edit_subscription_object;                       
                        return $http.post(url, rest_data,config);
                    },
                    
                    
                    
                }
            }]);