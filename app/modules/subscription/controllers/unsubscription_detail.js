'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('subscription')
        .controller('UnSubscriptionDetailController', ['$scope', '$rootScope', 'RestProduct', 'restAPISubscription', '$state', '$window',
            function($scope, $rootScope, RestProduct, restAPISubscription, $state, $window) {

                $rootScope.page_heading = "UnSubscription";
                $scope.subscriptionObj = {};
                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;        
                $scope.subscription_id = session.subscription_id;     
                $scope.subscription_params = {"customer_id":$scope.customer_id,"token":$scope.token};
                restAPISubscription.mysubscriptionDetail($scope.subscription_params).success(function(response)
                {
                    if (response.status == 'success') 
                    {                   
                        $scope.showLoader(true);
                        $scope.my_subscription = response.result.subscription_info; 
                        //$scope.subscriptionObj.subscription_pattern = response.result.subscription_info.subscription_pattern; 
                        //$scope.subscriptionObj.date = response.result.subscription_info.subscription_effect_from; 
                                          
                    }
                    else
                    {
                        $scope.showLoader(true);
                       // $state.go("home", {});
                        //alert(response.message);
                        $scope.showAlertMessage('Alert',response.message);

                        if (response.status === 'invalid_token') {
                             $rootScope.invalidToken();
                        }
                        
                        
                    }

                });

            }
        ]);
