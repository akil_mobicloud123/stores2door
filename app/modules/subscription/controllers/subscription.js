'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('core')
        .controller('SubscriptionController', ['$scope', '$rootScope', 'RestProduct', 'restAPISubscription', '$state', '$window', 'sendParamService', '$filter','$timeout','$modal','Constant',
            function($scope, $rootScope, RestProduct, restAPISubscription, $state, $window, sendParamService, $filter,$timeout,$modal,Constant) {

                $rootScope.page_heading = "Subscription";
                $scope.subscriptionObj = {};
                $scope.subscriptionObj.days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                $scope.mindate = new Date();
                $scope.subscriptionObj.date = $filter('date')($scope.mindate, "MM/dd/yyyy");
                
                $scope.ORDER_ACCEPT_CLOSE_AFTER = Constant.ORDER_ACCEPT_CLOSE_AFTER;  
                $scope.ORDER_ACCEPT_ALLOW_AFTER = Constant.ORDER_ACCEPT_ALLOW_AFTER;  


                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;


                // Selected days
                $scope.selection = [];
                $scope.dateOptions = {format: 'dd/mm/yyyy'}
                // Toggle selection for a given days by name
                $scope.toggleSelection = function toggleSelection(day) {
                    var idx = $scope.selection.indexOf(day);

                    // Is currently selected
                    if (idx > -1) {
                        $scope.selection.splice(idx, 1);
                    }

                    // Is newly selected
                    else {
                        $scope.selection.push(day);
                    }
                };

                /* 
                 * Get Milk Variant List 
                 */
                $scope.product_subscrib_array = new Array();
                $scope.productList = function() {

                    $scope.showLoader(false);

                    $scope.subscription_params = {"customer_id": $scope.customer_id, "token": $scope.token};
                    restAPISubscription.mysubscriptionDetail($scope.subscription_params).success(function(response)
                    {
                        if (response.status == 'success')
                        {
                            $scope.showLoader(true);
                            //$scope.my_subscription = response.result.subscription_info;
                            
                            if(response.totalCount!=0)
                            {
                                $scope.my_subscription = response.result.product_info;
                                for (var i = 0, len = $scope.my_subscription.length; i < len; i++) {
                                    // inner loop applies to sub-arrays

                                    $scope.product_subscrib_array.push($scope.my_subscription[i].product_id)

                                }
                            
                            }

                            //$scope.subscriptionObj.subscription_pattern = response.result.subscription_info.subscription_pattern; 
                            //$scope.subscriptionObj.date = response.result.subscription_info.subscription_effect_from; 

                        }
                        else
                        {
                            $scope.showLoader(true);
                            // $state.go("home", {});
                            //alert(response.message);
                        }

                    });

                    // loop
                    // Display Milk Variant Products Only
                    $scope.product_category = 2;

                    RestProduct.getProducList($scope.product_category).success(function(response)
                    {

                        $scope.status = response["status"];

                        //alert($scope.status);

                        if (response["status"] == "success")
                        {

                            $scope.totalCount = response["totalCount"];
                            $scope.product_list = response["result"];
                            $scope.showLoader(true);
                            $scope.no_list = false;
                        }
                        else
                        {
                            // $state.go("home");
                            $scope.no_list = true;
                            $scope.showLoader(true);
                            $scope.showAlertMessage('Alert',response["message"]); 

                            if (response["status"] === 'invalid_token') {
                                 $rootScope.invalidToken();
                            }
                            
                            
                        }

                    }).error(function(response) {
                        $scope.showLoader(true);
                        //alert(response.message);
                        //$state.go("home", {});
                    });

                };

                $scope.productList();
                /**
                 * Check product exist in product array
                 * @param {type} product_id
                 * @returns {Boolean}
                 */
                $scope.checkproduct_exist = function(product_id) {
                    
                   // console.log($scope.product_subscrib_array);
                    return $scope.product_subscrib_array.indexOf(product_id) == -1;
                }
                /*
                 * Submit Subscription Form
                 */
                $scope.subscription_submit = function(isValid) {
                    
                    // Check if Subscription Request is in Valid time
                    var today = new Date().getHours();
                    
                    $scope.t2 = $filter('date')(new Date(), "yyyy-MM-dd");
                    $scope.t1 = $filter('date')($scope.subscriptionObj.date, "yyyy-MM-dd");
                    
                    $scope.restrict_after = parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                    
                    //alert($scope.subscriptionObj.date+""+$scope.todays_date);
                    // Check if Order is in Valid Time Lock 
                    $scope.is_valid_time = $scope.time_lock_status($scope.t1,$scope.t2);
                    
                   
                    if ($scope.is_valid_time==1) 
                    {
                    
                        $scope.submitted = true;
                        if (isValid)
                        {
                            $rootScope.showLoader(false);

                            $scope.subscription_info = {"customer_id": $scope.customer_id, "token": $scope.token, "product_list": $rootScope.products_subscription, "effect_from": $scope.subscriptionObj.date, "array_custom_subscription": $rootScope.customize_products};
                            restAPISubscription.customerSubscriptionRegistation($scope.subscription_info).success(function(response) {
                                if (response.status == 'success')
                                {
                                    $scope.showLoader(true);
                                    //sendParamService.setId(id);
                                    var user_session = angular.fromJson($window.localStorage.getItem("user_session"));
                                    //console.log("===== Befor");
                                    user_session.subscription_id = response.result.subscription_id;

                                    var user_session = $window.localStorage.setItem("user_session", JSON.stringify(user_session));

                                    var user_session = angular.fromJson($window.localStorage.getItem("user_session"));

                                    $rootScope.products_subscription = [];

                                    $state.go("my_subscription_detail", {});
                                }
                                else
                                {
                                    $scope.showLoader(true);
                                   // alert(response.message);
                                  // bootbox.dialog({title: 'Alert', message: response.message });
                                    $scope.showAlertMessage('Alert',response.message);

                                    if (response.status === 'invalid_token') {
                                         $rootScope.invalidToken();
                                    }

                                }

                            }).error(function(response) {
                                $scope.showLoader(true);
                                //alert(response.message);
                                $state.go("home", {});
                            });
                        }

                    }
                    else
                    {
                        $scope.showAlertMessage('Time Over',"Subscription request not accepted between "+ $scope.restrict_after +":00 PM To 6:00 AM");
                    }

                };
                /**
                 * Make active days depeding on pattern 
                 * @param {type} $event
                 * @param {type} type
                 * @param {type} index
                 * @returns {undefined}
                 */
                $scope.mark_active_days_pattern = function($event, type,id) {                       
                    var index = $rootScope.CheckhasElement('product_id',id,$rootScope.products_subscription);                   
                    //option A - start manipulating in the dark:
                    angular.element($event.currentTarget).closest('.subscription-item').find('.Sun,.Mon,.Tue,.Wed,.Thu,.Fri,.Sat').parent().removeClass("active_day");
                    if(type=='daily')
                    {
                       angular.element($event.currentTarget).closest('.subscription-item').find('.Sun,.Mon,.Tue,.Wed,.Thu,.Fri,.Sat').parent().addClass("active_day");
                    }else if(type=='alternate')
                    {
                       angular.element($event.currentTarget).closest('.subscription-item').find('.Sun,.Tue,.Thu,.Sat').parent().addClass("active_day");
                    }else{
                      angular.element($event.currentTarget).closest('.subscription-item').find('.Mon,.Tue,.Wed,.Thu,.Fri').parent().addClass("active_day");  
                    }
                    angular.element($event.currentTarget).closest('.subscription-item').find('.subs-days').find('.day_quantity').html('0');
                    angular.element($event.currentTarget).closest('.subscription-item').find('.active_day').find('.day_quantity').html($rootScope.products_subscription[$rootScope.product_subscription_count].product_quantity);
                    
                }
                /**
                 * Check pattern exist in list else add 
                 * @param {type} id
                 * @param {type} price
                 * @param {type} quantity
                 * @param {type} image
                 * @param {type} product_weight
                 * @param {type} milk_type
                 * @param {type} subscription_pattern
                 * @returns {undefined}
                 */
                $scope.check_pattern = function(id, price, quantity, image, product_weight, milk_type, subscription_pattern) {

                    if ($rootScope.GetdataIfhasElement('product_id', id, $rootScope.products_subscription) != -1)
                    {
                        var index = $rootScope.CheckhasElement('product_id', id, $rootScope.products_subscription);
                        $rootScope.products_subscription[index].subscription_pattern = subscription_pattern;
                    } else {
                        $rootScope.products_subscription.push({product_id: id, product_rate: price, product_quantity: 1, product_image: image, product_weight: product_weight, product_total: price, subscription_pattern: subscription_pattern ,milk_type :milk_type,custmize_day :''});
                    }
                }
                /**
                 * Check days exist in list else add 
                 * @param {type} id
                 * @param {type} price
                 * @param {type} quantity
                 * @param {type} image
                 * @param {type} product_weight
                 * @param {type} milk_type
                 * @param {type} subscription_pattern
                 * @returns {undefined}
                 */
                $scope.subscription_day = function(id, price, quantity, image, product_weight, milk_type, subscription_day) {

                    if ($rootScope.GetdataIfhasElement('product_id', id, $rootScope.products_subscription) != -1)
                    {
                        var index = $rootScope.CheckhasElement('product_id', id, $rootScope.products_subscription);
                        $rootScope.products_subscription[index].subscription_day = subscription_day;
                    } else {
                        $rootScope.products_subscription.push({product_id: id, product_rate: price, product_quantity: 1, product_image: image, product_weight: product_weight, product_total: price, subscription_day: subscription_day,milk_type:milk_type,custmize_day :''});
                    }
                }
                $rootScope.customize_products = [];      
                 $scope.open_customize_popup = function(product_id, price, quantity, image, product_weight, milk_type, subscription_pattern) {                     
                    //$scope.items = ['item1', 'item2', 'item3'];
                    $scope.showLoader(false);
                    $scope.check_pattern(product_id, price, quantity, image, product_weight, milk_type, subscription_pattern);
                    console.log($rootScope.products_subscription);
                    var modalInstance = $modal.open({
                        backdrop: 'static', /*  this prevent user interaction with the background  */
                        keyboard: false,
                        templateUrl: 'modules/subscription/views/customize_subscription_product.html',
                        controller: 'CustomizeSubscriptionController',
                        resolve: {
                            product_id: function() {
                                return product_id;
                            },                            
                        },                        
                    });

                };
                
                
            }
        ])
         .controller('CustomizeSubscriptionController', ['$scope', '$rootScope', '$window','$state','$modal','$modalInstance','product_id',
            function($scope, $rootScope,$window,$state,$modal,$modalInstance,product_id) {
            
                $scope.showLoader(true);               
                $scope.cancel = function() {                        
                    $modalInstance.dismiss();                     
                };
               
                $scope.submit_custmize = function() {    
                    var index_custmize_array = $rootScope.CheckhasElement('product_id', product_id, $rootScope.products_subscription);    
					$rootScope.products_subscription[index_custmize_array].custmize_day = $scope.selection_day; 	
					$scope.sel_count = $rootScope.products_subscription[0].custmize_day.length;	
					if($scope.selection_day.length ==0 )
					{
						$scope.showAlertMessage('Alert',"Please select atleast one day.");	
					}
				    else
					{
						
						console.log($rootScope.products_subscription);
						$modalInstance.dismiss();  
					}	
                };
               
                /**
                 * 
                 * @param {type} product_id
                 * @returns {undefined}
                 */
                $scope.selection_day = []
                $scope.toggledaySelection = function toggledaySelection(day) {
                    var idx = $scope.selection_day.indexOf(day);

                    // Is currently selected
                    if (idx > -1) {
                        $scope.selection_day.splice(idx, 1);
                    }

                    // Is newly selected
                    else {
                        $scope.selection_day.push(day);
                    }
                    console.log($scope.selection_day);
                };
              
                console.log($scope.selection_day);
                
            }
        ])
        ;
