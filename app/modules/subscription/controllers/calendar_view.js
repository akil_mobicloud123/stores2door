
'use strict';

/**
 * @ngdoc object
 * @name subscription.Controllers.CalenderviewController
 * @description CalenderviewController controller
 * @requires ng.$scope
 * '$modalInstance',
 */
angular
        .module('subscription')
        .controller('CalenderviewController', ['$scope', '$rootScope', 'restAPISubscription','$window','RestProduct','$state','$modal',
            function($scope, $rootScope, restAPISubscription,$window,RestProduct,$state,$modal) {

                $rootScope.page_heading = "Calendar";
                $scope.calendersubscriptionObj = {};
                $scope.calendar_years =[];
                
               
                var curr_month_search = moment().format('MMMM');
                $scope.month_search = moment().month(curr_month_search).format("M");
                if($scope.month_search <= 9)
                {
                    $scope.month_search = '0'+$scope.month_search;
                }
                
                $scope.year_search =moment().format('YYYY');
               


                
                // Month Array
                $scope.calendar_month = [
                    {name: 'Jan', value: '01'},
                    {name: 'Feb', value: '02'},
                    {name: 'Mar', value: '03'},
                    {name: 'Apr', value: '04'},
                    {name: 'May', value: '05'},
                    {name: 'Jun', value: '06'},
                    {name: 'Jul', value: '07'},
                    {name: 'Aug', value: '08'},
                    {name: 'Sep', value: '09'},
                    {name: 'Oct', value: '10'},
                    {name: 'Nov', value: '11'},
                    {name: 'Dec', value: '12'},
                ];
                
                // Year Array
                for(var i_year= 2017;i_year<=2030;i_year++)
                {
                    $scope.calendar_years.push(i_year);
                }
               
		var myarray = {};	
                myarray.subscription_details = new Array();
                
                /*
                 * Function : get the Records for the Selected Month / Date
                 * @param {type} new_date
                 * @returns {undefined}
                 */
                $scope.subscription_date_change = function(new_date) 
		{
                    $scope.showLoader(false);			//alert(new_date);
                    var session = angular.fromJson($window.localStorage.getItem("user_session"));
                    $scope.customer_id = session.customer_id;
                    $scope.token = session.token;       
                    
                    $scope.record_for_month = $scope.year_search +"-"+$scope.month_search+"-01";

                    
                    
                    
                    $scope.subscription_info = {"customer_id": $scope.customer_id, "token": $scope.token, "todays_date": $scope.record_for_month};     
                    $scope.showLoader(false);
                    restAPISubscription.getMonthlySubscriptionDetail($scope.subscription_info).success(function(response) {
                        if (response.status == 'success') 
                        {
                            if(response.totalcount >0)
                            {
                                $scope.showLoader(true);
                                $scope.subscriptions_list = response.result; 
                                $scope.no_list = false; 
                                for (var j = 0; j < $scope.subscriptions_list.length; j++){
                                    }
//                                     myarray.subscription_details.push({
//                                            date: '2017/07/03',
//                                            value: 'Christmas Eve'                                     
//                                        });  
                                    //$scope.subscription_data = myarray.subscription_details;                                      
                            }
                            else
                            {   
                                $scope.subscriptions_list ="";
                                $scope.no_list = true; 
                                $scope.showLoader(true);

                            }
                        }
                        else
                        {
                           // $scope.no_list = true; 
                            $scope.showLoader(true);
                            //alert(response.message);
                            //bootbox.dialog({title: 'Alert', message: response.message}); 
                            $scope.showAlertMessage('Alert',response.message); 
                            if (response.status === 'invalid_token') {
                                $rootScope.invalidToken();
                            }
                            
                            
                        }

                    }).error(function(response) {
                        $scope.showLoader(true);
                        //alert(response.message);     
                        
                    });

                };
                

                $rootScope.$on("CallCalendarView", function(){
                    $scope.date = new Date();
                    $scope.subscription_date_change($scope.date);
                    
                });
                
                
                // Get Records Of Current Month
                $scope.date = new Date();
                $scope.subscription_date_change($scope.date);

                /**@function : edit_popup
                 * @description  Open Popup to Edit subscription Record
                 * @develop yogesh
                 */
                
                $scope.edit_popup = function(subscription_date,cust_id) {                     
                    
                        //$scope.items = ['item1', 'item2', 'item3'];
                        $scope.showLoader(false);
                        var modalInstance = $modal.open({
                            backdrop: 'static', /*  this prevent user interaction with the background  */
                            keyboard: false,
                            templateUrl: 'modules/subscription/views/calender_subscription_edit.html',
                            controller: 'CalenderEditSubscriptionController',
                            resolve: {
                                subscription_date: function() {
                                    return subscription_date;
                                },
                                cust_id: function() {
                                    return cust_id;
                                }, 
                            },                        
                        });

                };

                /**@function : view_subscription
                 * @description  Open Popup to View subscription Record
                 * @develop yogesh
                 */
                $scope.view_subscription = function(subscription_date,cust_id) {  
                    //$scope.items = ['item1', 'item2', 'item3'];
                     $scope.showLoader(false);
                    var modalInstance = $modal.open({
                        backdrop: 'static', /*  this prevent user interaction with the background  */
                        keyboard: false,
                        id : 'Myview',
                        templateUrl: 'modules/subscription/views/calender_subscription_view.html',
                        controller: 'CalenderViewSubscriptionController',
                        resolve: {
                             subscription_date: function() {
                                return subscription_date;
                            },
                            cust_id: function() {
                                return cust_id;
                            },
                        },
                        
                    });

                };
                
                
                
                /**@function : searchMonthRecord
                 * @description  searchMonthRecord Function
                 * @develop yogesh
                 */
                $scope.searchMonthRecord = function(isValid) {
                   
                    $scope.submitted = true;                
                    if (isValid)
                    {
                        $scope.showLoader(false);
                        //$scope.record_for_month = $scope.year_search +"-"+$scope.month_search;
                        $scope.subscription_date_change($scope.record_for_month);
                    }
                };
                
                /**@function : resetSearchRecord
                 * @description  Reset Search Resullt
                 * @develop yogesh
                 */
                $scope.resetSearchRecord = function() {
                   
                    var curr_month_search = moment().format('MMMM');
                    $scope.month_search = moment().month(curr_month_search).format("M");
                    if($scope.month_search <= 9)
                    {
                        $scope.month_search = '0'+$scope.month_search;
                    }

                    $scope.year_search =moment().format('YYYY');
                   
                    $scope.subscription_date_change($scope.record_for_month);
                    
                };
               
                
            }
        ])
        
        .controller('CalenderEditSubscriptionController', ['$scope', '$rootScope', 'restAPISubscription','$window','RestProduct','$state','$modal','subscription_date','$modalInstance','cust_id','$filter','Constant',
            function($scope, $rootScope, restAPISubscription,$window,RestProduct,$state,$modal,subscription_date,$modalInstance,cust_id,$filter,Constant) {

            var session = angular.fromJson($window.localStorage.getItem("user_session"));
            $scope.customer_id = session.customer_id;
            $scope.token = session.token;     
            
            $scope.calenderViewSubscriptionObj = {
                "customer_id":cust_id,
                "token":$scope.token,
                "todays_date":subscription_date,
                "for_selected_date":"1"                
            };
            
            restAPISubscription.viewProductSubscriptionDetails($scope.calenderViewSubscriptionObj).success(function(response)
            {
                
                $scope.status = response["status"];

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.subscriptions_details = response["result"][0]; 
                    $scope.product_detail_edit = response["result"][0].product;  
                    $scope.showLoader(true);
                }
                else
                {
                    // $state.go("home");
                    $scope.showLoader(true);
                    $scope.showAlertMessage('Alert',response["message"]); 
                    
                    if (response["status"] === 'invalid_token') {
                         $rootScope.invalidToken();
                    }
                     
                }

            }).error(function(response) {
                //alert("Error in connection.");
                //bootbox.dialog({title: 'Alert',message: "Error in connection "});
                $scope.showAlertMessage('Alert',"Error in connection."); 
                $scope.showLoader(true);	
            });

             $scope.cancel = function() {     
                    $modalInstance.dismiss();
                     
             };
             
            $scope.add_quantity = function(index,quantity) {  
           
                $scope.product_detail_edit[index].product_packet_quantity = parseInt(quantity) + 1;      
            };

// Wriiten By : Yogesh  , Commented By Prasad               
//            $scope.remove_product = function(product_id) {                   
//                $scope.product_detail_edit = $scope.product_detail_edit.filter((item) => item.product_id !== product_id);
//            };

            $scope.remove_product = function(index,quantity) {                   
                if($scope.product_detail_edit[index].product_packet_quantity > 1)
                {    
                    $scope.product_detail_edit[index].product_packet_quantity = parseInt(quantity) - 1;  
                }
            };
                
            $scope.edit_subscription_product = function() {  
                
                   $scope.showLoader(false); 
                    // console.log($scope.subscriptions_details);
                   $scope.edit_subscription_object = {'monthly_date': $scope.subscriptions_details.monthly_date, 'customer_id': $scope.customer_id, 'token': $scope.token,'product': $scope.product_detail_edit};
                  
                   // console.log($scope.edit_subscription_object);return false;
                   $modalInstance.dismiss();
                   // Check if Subscription Request is in Valid time
                    $scope.ORDER_ACCEPT_CLOSE_AFTER = Constant.ORDER_ACCEPT_CLOSE_AFTER;  
                    $scope.ORDER_ACCEPT_ALLOW_AFTER = Constant.ORDER_ACCEPT_ALLOW_AFTER;  
                    
                    var today = new Date().getHours();
                    
                    $scope.t2 = $filter('date')(new Date(), "yyyy-MM-dd");
                    $scope.t1 = $filter('date')($scope.subscriptions_details.monthly_date, "yyyy-MM-dd");
                    // Check if Order is in Valid Time Lock 
                    $scope.is_valid_time = $scope.time_lock_status($scope.t1,$scope.t2);                    

                    $scope.restrict_after = parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                    if ($scope.is_valid_time==1) 
                    {
                        restAPISubscription.updateSubscriptionProductQuantityForDay($scope.edit_subscription_object).success(function(response)
                        {

                            if (response["status"] == "success")
                            {
                                $rootScope.$emit("CallCalendarView", {}); 
                                //alert(response["message"]);
                                $scope.showAlertMessage('Success',response["message"]); 
                               $scope.showLoader(true); 
                            }
                            else
                            {
                                // $state.go("home");
                                $scope.showLoader(true);
                                $scope.showAlertMessage('Alert',response["message"]); 

                                if (response["status"] === 'invalid_token') {
                                     $rootScope.invalidToken();
                                }

                            }

                        }).error(function(response) {
                           // alert("Error in connection.");
                           //bootbox.dialog({title: 'Alert', message:"Error in connection"}); 
                           $scope.showAlertMessage('Alert',"Error in connection.");
                            $scope.showLoader(true);
                        });
                    }
                    else
                    {
                        $scope.showLoader(true); 
                        $scope.showAlertMessage('Time Over',"Subscription request not accepted between "+ $scope.restrict_after +":00 PM To 6:00 AM");
                    }
            };

            }
        ])
        .controller('CalenderViewSubscriptionController', ['$scope', '$rootScope', 'restAPISubscription','$window','RestProduct','$state','$modal','subscription_date','$modalInstance','cust_id','$filter',
            function($scope, $rootScope, restAPISubscription,$window,RestProduct,$state,$modal,subscription_date,$modalInstance,cust_id,$filter) {
            
            var session = angular.fromJson($window.localStorage.getItem("user_session"));
            $scope.customer_id = session.customer_id;
            $scope.token = session.token;     
           
            $scope.calenderViewSubscriptionObj = {
                "customer_id":cust_id,
                "token":$scope.token,
                "todays_date":subscription_date,
                "for_selected_date":"1"                
            };
            
            restAPISubscription.viewProductSubscriptionDetails($scope.calenderViewSubscriptionObj).success(function(response)
            {
                $scope.status = response["status"];

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_detail_view = response["result"][0].product;  
                    $scope.subscription_date_sel =subscription_date;
                    $scope.showLoader(true);
                }
                else
                {
                    // $state.go("home");
                     $scope.showLoader(true);
                    $scope.showAlertMessage('Alert',response["message"]); 

                    if (response["status"] === 'invalid_token') {
                         $rootScope.invalidToken();
                    }
                     
                }

            }).error(function(response) {
                //alert("Error in connection.");
                //bootbox.dialog({title: 'Alert',message: "Error in connection"}); 
                $scope.showAlertMessage('Alert',"Error in connection.");
                $scope.showLoader(true);	
            });

             $scope.cancel = function() {     
                    $modalInstance.dismiss();
                     
                };
                
                
                
            /*
             * Unsubscribe Product
             */
            $scope.funUnHoldSubscription = function(product_id) 
            {

                //if ($window.confirm("Do you really want to unhold this subscription?"))
                //{
                    bootbox.confirm({
                            message: "Do you really want to unhold this subscription?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }

                            },
                            closeButton:false,
                            callback: function (result) {
                                if(result==true){




                    $rootScope.showLoader(false);


                    $scope.subscription_info = {"customer_id": cust_id, "token": $scope.token, "product_id": product_id, "unhold_for_date":subscription_date};
                    $modalInstance.dismiss();
                    restAPISubscription.unHoldProductSubscription($scope.subscription_info).success(function(response) {

                        if (response.status == 'success')
                        {
                            $scope.showLoader(true);
                            //alert(response.message);
                            //bootbox.dialog({title: 'Alert',message:response.message}); 
                            $scope.showAlertMessage('Success',response.message);
                            $rootScope.$emit("CallCalendarView", {}); 
                        }
                        else
                        {
                            $scope.showLoader(true);
                            //alert(response.message);
                            //bootbox.dialog({title: 'Alert',message:response.message});
                            $scope.showAlertMessage('Alert',response.message);
                        }

                    }).error(function(response) {

                        $scope.showLoader(true);
                        //alert(response.message);
                        $state.go("home", {});
                    });
                //}
           console.log('This was logged in the callback: ' + result);
    }
    }
});

            };
            
            
            $scope.date_diff = function(t1, t2) {

                var one_day = 1000 * 60 * 60 * 24;

                var x = t1.split("-");
                var y = t2.split("-");

                var date1 = new Date(x[0], (x[1] - 1), x[2]);
                var date2 = new Date(y[0], (y[1] - 1), y[2]);

                var month1 = x[1] - 1;
                var month2 = y[1] - 1;

                var Diff = Math.ceil((date2.getTime() - date1.getTime()) / (one_day));

                return Diff * -1;

            };
            
            
            /**
             * Check Date Difference
             * @param {type} product_id
             * @returns {Boolean}
             */
            $scope.chkdate_diff = function(selected_date) {
                var curr_date = $filter('date')(new Date(), "yyyy-MM-dd");               
                
                var date_diff = $scope.date_diff(selected_date, curr_date);
                 
               // console.log($scope.product_subscrib_array);
                return date_diff;
            }

                
                
                
                
            }
        ])
        ;

