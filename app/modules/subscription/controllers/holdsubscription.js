'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('core')
        .controller('HoldsubscriptionController', ['$scope', '$rootScope', 'restAPISubscription', '$window', '$state','Constant','$filter',
            function($scope, $rootScope, restAPISubscription, $window, $state,Constant,$filter) {
                $rootScope.page_heading = "Hold Subscription";
                $scope.holdsubscriptionObj = {};
                $scope.hold_product = [];

                $scope.ORDER_ACCEPT_CLOSE_AFTER = Constant.ORDER_ACCEPT_CLOSE_AFTER;  
                $scope.ORDER_ACCEPT_ALLOW_AFTER = Constant.ORDER_ACCEPT_ALLOW_AFTER;  

                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;        
                $scope.subscription_id = session.subscription_id;     
                $scope.subscription_params = {"customer_id":$scope.customer_id,"token":$scope.token};
                
                restAPISubscription.mysubscriptionDetail($scope.subscription_params).success(function(response)
                {
                    if (response.status == 'success') 
                    {                   
                        $scope.showLoader(true);
                        $scope.my_subscription = response.result.product_info; 
                        
                        if($scope.my_subscription.length > 1 )
                        {
                            $scope.selectedAll = true; 
                            angular.forEach($scope.my_subscription, function(item2) { 
                                $scope.hold_product.push(item2.product_id);
                                item2.checked = true;  
                            });   
                        }else{
                            $scope.selectedAll = false;
                        }                       

                        //console.log($scope.my_subscription_info);
                       // $scope.subscription_info = $scope.my_subscription_info;
                       // console.log($scope.subscription_info);
                    }
                    else
                    {
                        $scope.showLoader(true);
                       // $state.go("home", {});
                        //alert(response.message);
                        $scope.showAlertMessage('Alert',response.message); 

                        if (response.status === 'invalid_token') {
                             $rootScope.invalidToken();
                        }
                        
                    }

                });

                // Toggle selection for a given days by name
                $scope.toggleSelection = function toggleSelection(product_id) {
                    
                    var pr_arr = $scope.hold_product;
                    var idx = pr_arr.indexOf(product_id.toString());
                    // Is currently selected
                    if (idx > -1) {
                         $scope.hold_product.splice(idx, 1);   
                         
                    }// Is newly selected
                    else {                        
                        $scope.hold_product.push(product_id);
                    }
                    if($scope.my_subscription.length == $scope.hold_product.length)
                    {
                         $scope.selectedAll = true;
                    }else{
                       $scope.selectedAll = false; 
                    }
                };
                /**
                 * Check select all functionality
                 */
                
                $scope.checkAll = function() {
                    $scope.hold_product = [];
                    //alert($scope.selectedAll);
                    if (!$scope.selectedAll) {
                        $scope.selectedAll = true;
                    } else {
                        $scope.selectedAll = false;
                    }
                   // alert($scope.selectedAll);
                    angular.forEach($scope.my_subscription, function(item2) {
                        
                           if($scope.selectedAll)
                           {
                                item2.checked = true;  
                                $scope.hold_product.push(item2.product_id);
                            }else{
                                item2.checked = false;  
                            }
                        
                    });                    
                }
               

                $scope.hold_subscription = function() 
                {      
                    
                    // Check if Subscription Request is in Valid time
                    var today = new Date().getHours();
  
                    
                    $scope.restrict_after =parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                     
                     
                    
                    //if (today >= $scope.ORDER_ACCEPT_ALLOW_AFTER && today < $scope.ORDER_ACCEPT_CLOSE_AFTER) 
                    
                        if ($scope.hold_product.length == 0)
                        {
                           // alert('Please select the product for hold the Subscription');
                           //bootbox.dialog({title: 'Alert',message:"Please select the product for hold the Subscription "});
                            $scope.showAlertMessage('Alert',"Please select the product for hold the Subscription ");
                            $scope.showLoader(true);
                            return false;
                        }

                        if (typeof $scope.holdsubscriptionObj.start_date === 'undefined' || $scope.holdsubscriptionObj.start_date === null)
                        {
                            //alert('Please select Start Date');
                             //bootbox.dialog({title: 'Alert',message:"Please select Start Date"});
                             $scope.showAlertMessage('Alert',"Please select the Start Date");
                            $scope.showLoader(true);
                            return false;
                        }
                        if (typeof $scope.holdsubscriptionObj.end_date === 'undefined' || $scope.holdsubscriptionObj.end_date === null)
                        {
                            //alert('Please select End Date');
                            //alert("Please select End Date");
                             $scope.showAlertMessage('Alert',"Please select End Date");
                            $scope.showLoader(true);
                            return false;
                        }
                        var date_diff = $scope.date_diff($scope.holdsubscriptionObj.end_date, $scope.holdsubscriptionObj.start_date);


                        if (date_diff > 14)
                        {
                           // alert('Subscription can be hold for maximum 14 Days.');
                              // bootbox.dialog({title: 'Alert',message: "Subscription can be hold for maximum 14 Days."});
                             $scope.showAlertMessage('Alert',"Subscription can be hold for maximum 14 Days.");
                            $scope.showLoader(true);
                            return false;
                        }
                        else if (date_diff < 0)
                        {
                            //alert('Please select proper End Date .');
                            //bootbox.dialog({title: 'Alert',message:"Please select proper End Date ."});
                             $scope.showAlertMessage('Alert',"Please select proper End Date .");
                            $scope.showLoader(true);
                            return false;
                        }


                        $scope.t2 = $filter('date')(new Date(), "yyyy-MM-dd");
                        $scope.t1 = $filter('date')($scope.holdsubscriptionObj.start_date, "yyyy-MM-dd");

                        //alert($scope.holdsubscriptionObj.start_date);
                        
                        // Check if Order is in Valid Time Lock 
                        $scope.is_valid_time = $scope.time_lock_status($scope.t1,$scope.t2);



                        if ($scope.is_valid_time==1) 
                        {

                            bootbox.confirm({
                                message: "1 : Are you sure you want to hold subscription?",
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }

                                },
                                closeButton:false,
                                callback: function (result) {
                                    if(result==true){

                               // if ($window.confirm("2 : Are you sure you want to hold subscription?"))
                                //{
                                    bootbox.confirm({
                                message: "2 : Are you sure you want to hold subscription?",
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }

                                },
                                closeButton:false,
                                callback: function (result) {
                                    if(result==true){

                                    //if ($window.confirm("1 : Are you sure you want to hold subscription?"))
                                    //{
                                        bootbox.confirm({
                                message: "3 : Are you sure you want to hold subscription?",
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }

                                },
                                closeButton:false,
                                callback: function (result) {
                                    if(result==true){

                                        $rootScope.showLoader(false);
                                        var session = angular.fromJson($window.localStorage.getItem("user_session"));
                                        $scope.customer_id = session.customer_id;
                                        $scope.token = session.token;
                                        $scope.subscription_id = session.subscription_id;

                                        $scope.hold_subscription_info = {"customer_id": $scope.customer_id, "token": $scope.token, "hold_from_date": $scope.holdsubscriptionObj.start_date, "hold_to_date": $scope.holdsubscriptionObj.end_date, "subscription_id": $scope.subscription_id,"product_list" :$scope.hold_product};
                                        restAPISubscription.holdSubscription($scope.hold_subscription_info).success(function(response) {
                                            if (response.status == 'success') {
                                                $scope.showLoader(true);
                                                //alert(response.message);
                                               // bootbox.dialog({title: 'Alert', message: response.message });
                                               $scope.showAlertMessage('Success',response.message);
                                                $state.go("calenderView", {});
                                            }
                                            else
                                            {
                                                $scope.showLoader(true);
                                                //alert(response.message);
                                                //bootbox.dialog({title: 'Alert', message: response.message });
                                                $scope.showAlertMessage('Alert',response.message);
                                                if (response.status === 'invalid_token') {
                                                     $rootScope.invalidToken();
                                                }



                                            }

                                        }).error(function(response) {
                                            $scope.showLoader(true);
                                            //alert(response.message);
                                            //bootbox.dialog({title: 'Alert', message: response.message });
                                            $scope.showAlertMessage('Alert',response.message);
                                            $state.go("home", {});
                                        });
                                    //}
                                      console.log('This was logged in the callback: ' + result);
                                        }
                                        }
                                    });
                                //}
                                  console.log('This was logged in the callback: ' + result);
                                }
                                }
                            });

                            //}
                            console.log('This was logged in the callback: ' + result);
                            }
                            }
                        });

                        }
                        else
                        {
                            $scope.showAlertMessage('Time Over',"Request not accepted between  "+ $scope.restrict_after +":00 PM To 6:00 AM.");
                        }


                };

                $scope.date_diff = function(t1, t2) {

                    var one_day = 1000 * 60 * 60 * 24;

                    var x = t1.split("-");
                    var y = t2.split("-");

                    var date1 = new Date(x[0], (x[1] - 1), x[2]);
                    var date2 = new Date(y[0], (y[1] - 1), y[2]);

                    var month1 = x[1] - 1;
                    var month2 = y[1] - 1;

                    var Diff = Math.ceil((date2.getTime() - date1.getTime()) / (one_day));

                    return Diff * -1;

                };

            }
        ]);
