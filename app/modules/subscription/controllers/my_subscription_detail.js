'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('subscription')
        .controller('MySubscriptionDetailController', ['$scope', '$rootScope', 'RestProduct', 'restAPISubscription', '$state', '$window','$modal','Constant',
            function($scope, $rootScope, RestProduct, restAPISubscription, $state, $window,$modal,Constant) {

                $rootScope.page_heading = "My Subscription";
                $scope.subscriptionObj = {};
                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;
                $scope.subscription_id = session.subscription_id;
                
                $scope.ORDER_ACCEPT_CLOSE_AFTER = Constant.ORDER_ACCEPT_CLOSE_AFTER;  
                $scope.ORDER_ACCEPT_ALLOW_AFTER = Constant.ORDER_ACCEPT_ALLOW_AFTER;  
                

                // Load My Subscription Detail
                $scope.fun_getMySubscription = function() {
					$scope.showLoader(false);
                    $scope.subscription_params = {"customer_id": $scope.customer_id, "token": $scope.token};
                    restAPISubscription.mysubscriptionDetail($scope.subscription_params).success(function(response)
                    {
                        if (response.status == 'success')
                        {
                            $scope.showLoader(true);
							if(response.totalCount >0)
							{
								$scope.my_subscription = response.result.product_info;
								//console.log($scope.my_subscription);
								//$scope.subscriptionObj.subscription_pattern = response.result.subscription_info.subscription_pattern; 
								//$scope.subscriptionObj.date = response.result.subscription_info.subscription_effect_from; 
							}
							else
							{
								$scope.my_subscription = "";
							}
                        }
                        else
                        {
                            $scope.showLoader(true);
                            // $state.go("home", {});
                            //alert(response.message);
                            $scope.showAlertMessage('Alert',response.message); 

                            if (response.status === 'invalid_token') {
                                 $rootScope.invalidToken();
                            }
                            
                            
                        }

                    });

                };



                /*
                 * Unsubscribe Product
                 */
                $scope.fun_unsubscribe_product = function(product_id, subscription_id) {
                    
                    // Check if Subscription Request is in Valid time
                    var today = new Date().getHours();
                    
                    $scope.restrict_after =parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                    
                    if (today >= $scope.ORDER_ACCEPT_ALLOW_AFTER && today < $scope.ORDER_ACCEPT_CLOSE_AFTER) 
                    {
                    

                       // if ($window.confirm("Do you really want to unsubscribe this product?"))
                       // {
                        bootbox.confirm({
                            message: "Do you really want to unsubscribe this product?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }

                            },
                            closeButton:false,
                            callback: function (result) {
                                if(result==true){



                            $rootScope.showLoader(false);


                            $scope.subscription_info = {"customer_id": $scope.customer_id, "token": $scope.token, "product_id": product_id, "subscription_id": subscription_id};

                            restAPISubscription.unsubscribeProduct($scope.subscription_info).success(function(response) {

                                if (response.status == 'success')
                                {
                                    $scope.showLoader(true);
                                    //alert(response.message);
                                   // bootbox.dialog({title: 'Alert', message: response.message });
                                   $scope.showAlertMessage('Success',response.message);
                                    //$state.go("my_subscription_detail", {});
                                    $scope.fun_getMySubscription();
                                }
                                else
                                {
                                    $scope.showLoader(true);
                                    //alert(response.message);
                                    //bootbox.dialog({title: 'Alert', message: response.message });
                                    $scope.showAlertMessage('Alert',response.message); 

                                    if (response.status === 'invalid_token') {
                                         $rootScope.invalidToken();
                                    }


                                }

                            }).error(function(response) {

                                $scope.showLoader(true);
                                //alert(response.message);
                                //bootbox.dialog({title: 'Alert', message: response.message });
                                $scope.showAlertMessage('Alert',response.message);
                                $state.go("home", {});
                            });
                        //}
                        console.log('This was logged in the callback: ' + result);
    }
    }
});

                    }
                    else
                    {
                        $scope.showAlertMessage('Time Over',"Request not accepted between  "+ $scope.restrict_after +":00 PM To 6:00 AM.");
                    }

                };



                /* Open Modify Popup to Edit Quantity
                 * @param {type} subscription_id
                 * @param {type} product_id
                 * @returns {undefined}
                 */

                $scope.modify_popup = function(product_id,subscription_id) {                     

                    // Check if Subscription Request is in Valid time
                    var today = new Date().getHours();
                    
                    $scope.restrict_after =parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                    
                    if (today >= $scope.ORDER_ACCEPT_ALLOW_AFTER && today < $scope.ORDER_ACCEPT_CLOSE_AFTER) 
                    {
                    
                        $scope.showLoader(false);
                        var modalInstance = $modal.open({
                            backdrop: 'static', /*  this prevent user interaction with the background  */
                            keyboard: false,
                            templateUrl: 'modules/subscription/views/modify_subscription_view.html',
                            controller: 'MysubscriptionModifySubscriptionController',
                            resolve: {
                                product_id: function() {
                                    return product_id;
                                },
                                subscription_id: function() {
                                    return subscription_id;
                                }, 
                            },                        
                        });
                    }
                    else
                    {
                        $scope.showAlertMessage('Time Over',"Request not accepted between  "+ $scope.restrict_after +":00 PM To 6:00 AM.");
                    }

                };
                
                
                // Call this on Modal Close 
                $rootScope.$on("CallCalendarView", function(){
                    $scope.fun_getMySubscription();
                    
                });



                // Load My Subscription
                $scope.fun_getMySubscription();


            }
        ])



        .controller('MysubscriptionModifySubscriptionController', ['$scope', '$rootScope', 'restAPISubscription','$window','RestProduct','$state','$modal','product_id','$modalInstance','subscription_id',
            function($scope, $rootScope, restAPISubscription,$window,RestProduct,$state,$modal,product_id,$modalInstance,subscription_id) {

            var session = angular.fromJson($window.localStorage.getItem("user_session"));
            $scope.customer_id = session.customer_id;
            $scope.token = session.token;     
            
            $scope.calenderViewSubscriptionObj = {
                "customer_id":$scope.customer_id,
                "token":$scope.token,
                "subscription_id":subscription_id,
                "product_id":product_id              
            };
            
            restAPISubscription.getProductSubscriptionDetails($scope.calenderViewSubscriptionObj).success(function(response)
            {
                
                $scope.status = response["status"];

                if (response["status"] == "success")
                {

                    $scope.totalCount = response["totalCount"];
                    $scope.product_detail_edit = response["result"][0].product;  
                    $scope.showLoader(true);
                }
                else
                {
                    // $state.go("home");
                     $scope.showLoader(true);
                }

            }).error(function(response) {
                //alert("Error in connection.");
                $scope.showLoader(true);	
            });

             $scope.cancel = function() {     
                    $modalInstance.dismiss();
                     
             };
             
            $scope.add_quantity = function(index,quantity) {  
                
                $scope.product_detail_edit[index].product_packet_quantity = parseInt(quantity) + 1;      
            };


            $scope.remove_product = function(index,quantity) {                   
                
                if($scope.product_detail_edit[index].product_packet_quantity > 1)
                {    
                    $scope.product_detail_edit[index].product_packet_quantity = parseInt(quantity) - 1;  
                }
            };
                
            $scope.modify_subscription_product = function() {  
                
                   $scope.showLoader(false); 
                    // console.log($scope.subscriptions_details);
                   $scope.edit_subscription_object = {'customer_id': $scope.customer_id, 'token': $scope.token,'product': $scope.product_detail_edit};
                  
                   // console.log($scope.edit_subscription_object);return false;
                   $modalInstance.dismiss();
                    restAPISubscription.modifySubscriptionProductQuantityForMonth($scope.edit_subscription_object).success(function(response)
                    {

                        if (response["status"] == "success")
                        {
                           $rootScope.$emit("CallCalendarView", {}); 
                           //alert(response["message"]);
                           //bootbox.dialog({title: 'Alert', message: response.message });
                           $scope.showAlertMessage('Success',response.message);

                           $scope.showLoader(true); 
                        }
                        else
                        {
                            // $state.go("home");
                            $scope.showLoader(true);
                            
                        }

                    }).error(function(response) {
                        //alert("Error in connection.");
                        //bootbox.dialog({title: 'Alert', message: "Error in connection" });
                        $scope.showAlertMessage('Alert',"Error in connection");
                        $scope.showLoader(true);
                    });
            };

            }
        ]);
