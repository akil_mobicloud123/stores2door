'use strict';

/**
 * @ngdoc service
 * @name core.Services.Rest
 * @description Rest Factory
 */
angular
        .module('core')
        .factory('restAPICore', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    /**
                     * Login
                     * @param {type} user
                     * @param {type} password
                     * @returns {unresolved}
                     */
                    customerLogin: function (mobile_no) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'customerLogin';
                        var config = {};
                        var rest_data = {                     
                            'mobile_no': mobile_no,                                             
                        };                       
                        return $http.post(url, rest_data,config);
                    },

                    getCategoryList: function () {
                        // Set the Content-Type 
                             
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCategoryList';
                        
                        var config = {};
                        var rest_data = {};
                        return $http.post(url, rest_data,config);
                    },

                    getSubCategoryList: function (product_category) {
                        // Set the Content-Type 
                       
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getSubCategoryList';
                        
                        var config = {};
                        var rest_data = {"category_id" : product_category};
                        return $http.post(url, rest_data,config);
                    },

                    registerCustomer: function (userObj) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'registerCustomer';
                        
                        var config = {};
                        var rest_data = userObj;                       
                        return $http.post(url, rest_data,config);
                    },
                    verifyOtpNumber: function (mobile_no,otp) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'verifyOtpNumber';
                        
                        var config = {};
                        var rest_data = {                     
                            'mobile_no': mobile_no,      
                            'otp': otp,   
                        };                        
                        return $http.post(url, rest_data,config);
                    },
                    getLocationsList: function (city_id) {
                      
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getLocationsList';
                        
                        var config = {};
                        var rest_data = {
                            'city_id':city_id
                        };
                        return $http.post(url, rest_data,config);
                    },
                     getCustomerDetail: function (cust_info) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCustomerDetail';
                        
                        var config = {};
                        var rest_data = cust_info;
                        return $http.post(url, rest_data,config);
                    },
                    updateCustomerProfile: function (cust_info) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        var url = urlBase + 'updateCustomerProfile';
                        
                        var config = {};
                        var rest_data = cust_info;
                        return $http.post(url, rest_data,config);
                    },

                    getCityList: function () {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getCityList';
                        
                        var config = {};
                        var rest_data = {};
                        return $http.post(url, rest_data,config);
                    },
                    resendOtpVal: function (mobile_no) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'resendOtpVal';
                        
                        var config = {};
                        var rest_data = {                     
                            'mobile_no': mobile_no
                        };                        
                        return $http.post(url, rest_data,config);
                    },
					
                    getDashboardCount: function (cust_info) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getDashboardCount';
                        
                        var config = {};
                        var rest_data = cust_info;
                        return $http.post(url, rest_data,config);
                    },
                    saveDeviceInfo: function (device_object) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'saveDeviceInfo';
                        
                        var config = {};
                        var rest_data = device_object;
                        return $http.post(url, rest_data,config);
                    },
					
					
                    
                }
            }]);