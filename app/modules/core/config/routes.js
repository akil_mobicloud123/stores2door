'use strict';

/**
 * @ngdoc object
 * @name core.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the core module
 */
angular
        .module('core')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name core.config.route
                 * @eventOf core.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider


                        .state('home', {
                            url: '/',
                            templateUrl: 'modules/core/views/home.html',
                            controller: 'HomeController'
                        })

                        .state('login', {
                            url: '/login',
                            templateUrl: 'modules/core/views/login.html',
                            controller: 'LoginController'
                        })

                        .state('logout', {
                            url: '/logout',
                            templateUrl: 'modules/core/views/login.html',
                            controller: 'LoginController'
                        })

                        .state('otp', {
                            url: '/otp',
                            templateUrl: 'modules/core/views/otp.html',
                            controller: 'OtpController'
                        })
                        .state('enterOtp', {
                            url: '/enterOtp/:otp_for?',
                            templateUrl: 'modules/core/views/enterOtp.html',
                            controller: 'EnterotpController'
                        })
                        .state('referralCode', {
                            url: '/referralCode',
                            templateUrl: 'modules/core/views/referralCode.html',
                            controller: 'ReferralcodeController'
                        })
                        .state('userinfo', {
                            url: '/userinfo',
                            templateUrl: 'modules/core/views/userinfo.html',
                            controller: 'UserinfoController'
                        })

                        .state('termsofuse', {
                            url: '/termsofuse',
                            templateUrl: 'modules/core/views/termsofuse.html',
                            controller: 'TermsofuseController'
                        })
                        .state('howitworks', {
                            url: '/howitworks',
                            templateUrl: 'modules/core/views/howitworks.html',
                            controller: 'HowitworksController'
                        })
                        .state('customercare', {
                            url: '/customercare',
                            templateUrl: 'modules/core/views/customercare.html',
                            controller: 'CustomercareController'
                        })


                        .state('registration', {
                            url: '/registration',
                            templateUrl: 'modules/core/views/registration.html',
                            controller: 'RegistrationController'
                        })

                        .state('update_profile', {
                            url: '/update_profile',
                            templateUrl: 'modules/core/views/update_profile.html',
                            controller: 'UpdateProfileController'
                        })

                     
                        .state('offers', {
                            url: '/offers',
                            templateUrl: 'modules/core/views/offers.html',
                            controller: 'OffersController'
                        })

                        .state('best-sellers', {
                            url: '/best-sellers',
                            templateUrl: 'modules/core/views/best-sellers.html',
                            controller: 'BestSellersController'
                        })


                        .state('our-recommandations', {
                            url: '/our-recommandations',
                            templateUrl: 'modules/core/views/our-recommandations.html',
                            controller: 'OurRecommandationsController'
                        })


            }
        ]);
