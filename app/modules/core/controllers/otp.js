'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('OtpController', ['$scope','$rootScope','restAPICore','$window','$state',
        function($scope,$rootScope,restAPICore,$window,$state) {
            
           $rootScope.menuShow =true; 
           $rootScope.addToCartShow =true; 
           $rootScope.page_heading = "Verify OTP";  
            /**
             * @description  Otp Function
             * @develop yogesh
             */
            var session = angular.fromJson($window.localStorage.getItem("otp_details"));
            $scope.ResendOtp = function(isValid) {
                debugger
                    $scope.submitted = true;      
                    if (isValid)
                    {
                       
                        var mobile_no = session.mobile_no;
                        var otp = session.otp;
                        restAPICore.verifyOtpNumber(mobile_no,otp).success(function(response) {
                            debugger
                            if (response.status == 'success') {
                                $window.localStorage.removeItem("otp_details"); 
                                // Store user info in Session     
                                 $rootScope.user_detail = {
                                    "customer_name": response.result.customer_name,
                                    "mobile_no": response.result.mobile_no,          
                                    "token": response.result.token,                                   
                                };                               
                                $window.localStorage.setItem("user_session", angular.toJson($rootScope.user_detail));   
                                
                                $window.sessionStorage.otp = response.result.otp;
                                $state.go("enterOtp", {});
                            }
                            else
                            {
                                debugger
                                $scope.showLoader(false);
                                //alert(response.message); 
                               // bootbox.dialog({title: 'Welcome', message: response.message });  
                               $scope.showAlertMessage('Alert',response.message);                                
                                $state.go("home", {});
                            }

                        }).error(function(response) {
                            //alert(response.message);  
                           // bootbox.dialog({title: 'Welcome', message: response.message }); 
                           $scope.showAlertMessage('Alert',response.message);                                
                            $state.go("home", {});
                        });
                       
                    }

            };

        }
    ]);
