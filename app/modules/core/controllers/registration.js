'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('core')
        .controller('RegistrationController', ['$scope', 'restAPICore', '$state','$rootScope','$window','Constant',
            function($scope, restAPICore, $state,$rootScope,$window,Constant) {

                $rootScope.page_heading = "Registration";
                $rootScope.addToCartShow =true; 
                $rootScope.menuShow=true;
		
                $rootScope.showLoader(false);
                
                /**
                 * Get the location from location API
                 */
                $scope.getSelectedCityLocations= function(city_id){
                       debugger
                    restAPICore.getLocationsList(city_id).success(function(response) {  
                      
                        $scope.locations = response.result;
                      }).error(function(response) {
                    });
    
                };

                /**
                 * Get the City List
                 */
                restAPICore.getCityList().success(function(response) {  
                    $scope.city_list = response.result;
                    $rootScope.showLoader(true);
                  }).error(function(response) {
                     $rootScope.showLoader(true);  
                     $scope.showAlertMessage('Alert',"Error in connection");
                });
                
                /**
                 * @description  Signup Function
                 * @develop yogesh
                 */
                $scope.userObj = {};
		$scope.userObj.city_id =1;
                $scope.SignUp = function(isValid) {
                    $scope.submitted = true;
                    if (isValid)
                    {  
                        
                        
                        $rootScope.showLoader(false);
                        restAPICore.registerCustomer($scope.userObj).success(function(response) {
                            debugger
                            if (response.status == 'success') 
                            {
                                // Store user info in Session  
                                $rootScope.user_detail = {
                                    "mobile_no": response.result.mobile_no,
                                    "otp": response.result.otp,
                                };   
                                
                                if (Constant.DEVICE_PLATFORM === "Android")
                                {     
                                    $scope.device_info = {
                                                    'Device Model':device.model ,
                                                    'Cordova Version On Device ':  device.cordova ,
                                                    'Device Platform' : device.platform,
                                                    'Device UUID ' : device.uuid , 
                                                    'Device OS Version': device.version ,
                                                    'Device Manufacturer' : device.manufacturer ,
                                                    'Device Hardware SerialNo ' : device.serial
                                                };

                                    $rootScope.device_info =  JSON.stringify($scope.device_info);

                                    $scope.device_object = {'customer_id': response.result.customer_id, 'uuid': device.uuid, 'platform': device.platform, 'version': device.version}

                                    restAPICore.saveDeviceInfo($scope.device_object).success(function(data)
                                    {

                                    }).error(function(error) {

                                    });
                                }  
                                
                                
                                $window.localStorage.removeItem("otp_details"); 
                                $window.localStorage.setItem("otp_details", angular.toJson($rootScope.user_detail));                                  
                                var session = angular.fromJson($window.localStorage.getItem("otp_details"));               
                                $scope.showLoader(true);
                                $state.go("enterOtp", { 'otp_for': 'sign_up'});
                                
                            }
                            else
                            {
                               $scope.showLoader(true);
                               //alert(response.message);
                              // bootbox.dialog({title: 'Alert', message: response.message });
                              $scope.showAlertMessage('Alert',response.message);    
                               //$state.go("home", {});
                            }

                        }).error(function(response) {
                            $scope.showLoader(true);
                           // alert(response.message);
                           //bootbox.dialog({title: 'Alert', message: response.message }); 
                           //$scope.showAlertMessage('Alert',response.message);   
                           $scope.showAlertMessage('Alert',"Error in connection");
                            $state.go("home", {});
                        });
                    }

                };
                
                
                
                /* 
                * Open Term & Condition in Webview 
                */

                $scope.openTermsPrivacy = function(link_for) 
                {
                    
                    if(link_for=="term")
                    {
                        var open_link = "http://qunova.com/termsofservice/";
                        
                    }
                    else
                    {
                         var open_link = "http://qunova.com/privacypolicy/";
                    }



                    if (Constant.DEVICE_PLATFORM === "web")
                    {
                            $window.open(open_link);
                           // $scope.showLoader(false);
                    }
                    else if (Constant.DEVICE_PLATFORM === "Android")
                    {
                           // var options = {location: 'yes', EnableViewPortScale: 'yes', toolbar: 'yes'}; 

                            var ref = cordova.InAppBrowser.open(open_link, '_blank', 'location=yes,EnableViewPortScale=yes,toolbar=no');
                          //  $scope.showLoader(true);
                    }

                };
                
                
                
            }
        ]);
