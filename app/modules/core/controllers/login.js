'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('LoginController', ['$scope','$rootScope','restAPICore','$window','$state',
        function($scope,$rootScope,restAPICore,$window,$state) {
           
           $rootScope.menuShow =true; 
           $rootScope.addToCartShow =true; 
           $rootScope.page_heading ="Login";
            /**
             * @description  Login Function
             * @develop yogesh
             */
            $scope.Login = function(isValid) {
                    $scope.submitted = true;                
                    if (isValid)
                    {
                        $scope.showLoader(false);
                       restAPICore.customerLogin($scope.mobile_no).success(function(response) {
                            if (response.status == 'success') {
                                // Store user info in Session        
                                $rootScope.user_detail = {
                                    "mobile_no": $scope.mobile_no,
                                    "otp": response.result.otp,
                                };   
                                $window.localStorage.setItem("otp_details", angular.toJson($rootScope.user_detail));  
                                $state.go("enterOtp", { 'otp_for': 'sign_in'});
                            }
                            else
                            {
                                $scope.showLoader(true);                                
                                $scope.mobile_no ="";
                                //alert(response.message); 
                                //bootbox.dialog({title: 'Alert', message: response.message });
                                // $scope.showAlertMessage('Alert',response.message);                                
                              // $state.go("home", {});
                            }

                        }).error(function(response) {
                            //alert(response.message);  
                           // bootbox.dialog({title: 'Alert', message: response.message }); 
                           $scope.showAlertMessage('Alert',"Error in connection");          
                           $scope.showLoader(true);    
                            //$state.go("home", {});
                        });
                       
                    }

            };
        }
    ]);
