'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('EnterotpController', ['$scope','$rootScope','restAPICore','$window','$state',
        function($scope,$rootScope,restAPICore,$window,$state) {
            
           $rootScope.menuShow =true; 
           $rootScope.addToCartShow =true; 
           $rootScope.page_heading = "Verify OTP";  
            
           /**
             * @description  Otp Function
             * @develop yogesh
             */
            var session = angular.fromJson($window.localStorage.getItem("otp_details"));
            $scope.mobile_no = session.mobile_no;
            $scope.otp_no = session.otp;
                        
			
			/*
			* Function to Verify the OTP Number
			* Date : 10/06/2017
			*/
						
            $scope.VerifyOtp = function(isValid) {
                    $scope.submitted = true;      
					$scope.otp_for = $state.params.otp_for;
					 
                    if (isValid)
                    {
                        $scope.showLoader(false);
                        restAPICore.verifyOtpNumber($scope.mobile_no,$scope.otp_no,$scope.otp_for).success(function(response) {
                            debugger
                            if (response.status == 'success') {
                                $window.localStorage.removeItem("otp_details"); 
                                // Store user info in Session     
                                 $rootScope.user_detail = {
                                    "customer_id": response.result.customer_id,
                                    "customer_name": response.result.customer_name,
                                    "mobile_no": response.result.mobile_no,          
                                    "token": response.result.token,   
                                    "subscription_id": response.result.subscription_id,
                                    "minimum_order_amount": response.result.minimum_order_amount,
                                    "delivery_charges": response.result.delivery_charges,
                                };                               
                                $window.localStorage.setItem("user_session", angular.toJson($rootScope.user_detail));   
                                
                                $window.sessionStorage.otp = response.result.otp;
				$scope.showLoader(true);
                                //alert(response.message);
                               //bootbox.alert({title: 'Welcome', message: response.message });
                               // bootbox.dialog({title: 'Welcome', message: response.message });
                               $scope.showAlertMessage('Welcome',response.message);
                                $state.go("home", {});
                            }
                            else
                            {
                                $scope.showLoader(true);
                               // alert(response.message);
                               // $state.go("home", {});
                               // bootbox.dialog({title: 'Alert', message: response.message }); 
                                $scope.showAlertMessage('Alert',response.message);   
                            }

                        }).error(function(response) {

                        });
                       
                    }

            };
			





			/*
			* Function to Resend the OTP Number
			* Date : 29/06/2017
			*/
						
            $scope.resendOtp = function(isValid) {
				
                    $scope.submitted = true;      
					$scope.otp_for = $state.params.otp_for;
					 
                 
                        $scope.showLoader(false);
                        restAPICore.resendOtpVal($scope.mobile_no).success(function(response) {
  
                              if (response.status == 'success') {
                                // Store user info in Session        
                                $rootScope.user_detail = {
                                    "mobile_no": $scope.mobile_no,
                                    "otp": response.result.otp,
                                };   
								$scope.otp_no = response.result.otp;
                                $window.localStorage.setItem("otp_details", angular.toJson($rootScope.user_detail));  
                               // alert("New OTP has been on your mobile no.");
                                //bootbox.dialog({title: 'Info', message: "New OTP is sent on your mobile no." });    
                                
                                $scope.showAlertMessage('Resend OTP',"New OTP has been sent on your registred  mobile number.");
				$scope.showLoader(true); 
                            }
                            else
                            {
                                $scope.showLoader(true);                                
                                $scope.mobile_no ="";
                                //alert(response.message); 
                                //bootbox.dialog({title: 'Alert', message: response.message });                               
                              // $state.go("home", {});
                                $scope.showAlertMessage('Alert',response.message);
                            }



                        }).error(function(response) {

                        });
                       
                 

            };
			


			
            
        }
    ]);
