/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires '$scope','$rootScope','$state','restAPIService'
 */
angular.module('core').controller('HomeController', ['$scope','$cookies','$rootScope','$state','$window','Constant','$location','restAPICore',
    function($scope,$cookies,$rootScope,$state,$window,Constant,$location,restAPICore) {
		var user= $cookies.get('user');
                $rootScope.menuShow =false; 
                $rootScope.addToCartShow =false; 
                
                $rootScope.page_heading ="Stores2Door";
                
                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                
                if($scope.is_session ==true) 
                {
                    $scope.customer_id = session.customer_id;
                    $scope.token = session.token;           


                    $scope.cust_info = {"customer_id":$scope.customer_id,"token":$scope.token};
                
                    restAPICore.getDashboardCount($scope.cust_info).success(function(response)
                    {

                        if (response["status"] == "success")
                        {
                            $scope.OutStandingAmount = response["result"]["OutStandingAmount"];
                            $scope.payment_val = response["result"]["payments"];
                            
                            if($scope.payment_val!="")
                            {
                                // LATEST PAYMENT
                                $scope.latest_payment = $scope.payment_val[0]['payment'];
                                $scope.latest_payment_date = $scope.payment_val[0]['date'];

                                // LAST PAYMENT
                                $scope.last_payment = $scope.payment_val[1]['payment'];
                                $scope.last_payment_date = $scope.payment_val[1]['date'];
                            }
                            else
                            {
                                $scope.latest_payment = "--";
                                $scope.last_payment = "--";
                            }    
                            
                            
                            $scope.showLoader(true);
                        }
                        else
                        {
                            $scope.showLoader(true);
                        }

                    }).error(function(response) {                
                        $scope.showLoader(true);
                    });
                }    
            $scope.getCat=function(){
                restAPICore.getCategoryList().success(function(response) {  
                  
                       $scope.product_category = response.result;
                     }).error(function(response) {
                   });

                   $scope.cat_id=10;
                   
                   restAPICore.getSubCategoryList($scope.cat_id).success(function(response) {  
                       
                       $scope.sub_category = response.result;
                     }).error(function(response) {
                   });
                   
            }

            $scope.getCat();

            $scope.getCatId=function(cat_id,cat_title)
            {
               
                //  $rootScope.catogory_id=cat_id;
                //  $rootScope.cat_title=cat_title;
                $window.localStorage.setItem("catogory_id",cat_id);
                $window.localStorage.setItem("catogory_title",cat_title);
                 $location.url('/sub_category');
    
            };

            $scope.getProductById=function(cat_id,cat_sub_id,cat_title)
            {
      
                //  $rootScope.catogory_id=cat_id;
                //  $rootScope.catogory_sub_id=cat_sub_id;
                //  $rootScope.catogory_title=cat_title;
              
                $window.localStorage.setItem("catogory_id",cat_id);
                $window.localStorage.setItem("catogory_sub_id",cat_sub_id);
                $window.localStorage.setItem("catogory_sub_title",cat_title);
                 $location.url('/Beauty_products');
    
            };
               
    }
])

angular.module('core').controller('ForgotPasswordController', ['$scope','$cookies','$rootScope','$state','$window','Constant',
    function($scope,$cookies,$rootScope,$state,$window,Constant) {
		
		var user= $cookies.get('user');
		$scope.userObj ={};
		
		if (user == "" || user == undefined) {
			$state.go('home');
		}else{
			$rootScope.user = JSON.parse(user);
			$window.sessionStorage.token = $rootScope.user.result.token;
		}
		
		if($state.current.name === 'logout'){
			logout();
		}
		
		
            
         /* 
         * 
         * @returns {undefined}
         */
            
          
    }
]);
