'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('UserinfoController', ['$scope','$rootScope','restAPICore','$window','$state',
        function($scope,$rootScope,restAPICore,$window,$state) {
           $rootScope.menuShow =true; 
           $rootScope.addToCartShow =true; 
           $rootScope.page_heading = "My Profile";  
           var session = angular.fromJson($window.localStorage.getItem("user_session"));
           
            if (!$window.localStorage.getItem("user_session")) 
            {
                $state.go("home");
            } 
            else
            {
                
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;           
                $scope.cust_info = {"customer_id":$scope.customer_id,"token":$scope.token};
                 /**
                 * Get the location from location API
                 */
                 restAPICore.getCustomerDetail($scope.cust_info).success(function(response) {

                     if(response.status == "success")
                     {

                         $scope.user_details = response.result; 
                     }
                     else
                     {
                         $scope.showAlertMessage("Alert",response.message); 
                         if (response.status === 'invalid_token') {
                             $rootScope.invalidToken();
                         }


                     }

                 }).error(function(response) {
                     //alert(response.message);                                
                     //bootbox.dialog({title: 'Alert', message: response.message }); 
                     $scope.showAlertMessage('Alert',response.message);   
                     $state.go("home", {});
                 });               
                
                
                
            }
           
           
           
            

        }
    ])
    
    .controller('UpdateProfileController', ['$scope', 'restAPICore', '$state','$rootScope','$window',
        function($scope, restAPICore, $state,$rootScope,$window) {

            $rootScope.page_heading = "Update Profile";
            $rootScope.addToCartShow =true; 
            $rootScope.menuShow=true;

            /**
             * Get the location from location API
             */
            restAPICore.getLocationsList().success(function(response) {  
                $scope.locations = response.result;
              }).error(function(response) {
                  // alert(response.message);                                
                  // $state.go("home", {});
            });

            /**
             * Get the City List
             */
            restAPICore.getCityList().success(function(response) {  
                $scope.city_list = response.result;
              }).error(function(response) {
                  //alert(response.message);                                
                  //$state.go("home", {});
            });
                
            /**
            * Get the location from location API
            */
           var session = angular.fromJson($window.localStorage.getItem("user_session"));
           $scope.customer_id = session.customer_id;
           $scope.token = session.token;           
           $scope.cust_info = {"customer_id":$scope.customer_id,"token":$scope.token};
           
            restAPICore.getCustomerDetail($scope.cust_info).success(function(response) {  
                $scope.userObj = response.result; 
            }).error(function(response) {
                //alert(response.message);  
                //bootbox.dialog({title: 'Alert', message: response.message });
                $scope.showAlertMessage('Alert',response.message);                               
                $state.go("home", {});                
            });
            
            /**
             * @description  Login Function
             * @develop yogesh
             */
            $scope.updateProfile = function(isValid) {
                    
                    $scope.submitted = true;                
                    if (isValid)
                    {

                        $scope.userObj['token'] = $scope.token;
                        restAPICore.updateCustomerProfile($scope.userObj).success(function(response) {

                            if (response.status == 'success') {

                               // alert(response.message);
                               //bootbox.dialog({title: 'Alert', message: response.message }); 
                               $scope.showAlertMessage('Success',response.message);
                                // Store user info in Session                                 
                                $state.go("userinfo", {});
                            }
                            else
                            {
                                $scope.showLoader(false);
                               // alert(response.message);
                                //bootbox.dialog({title: 'Alert', message: response.message }); 
                                $scope.showAlertMessage('Alert',response.message);
                                $state.go("home", {});
                            }

                        }).error(function(response) {
                            $scope.showLoader(false);
                            //alert(response.message);
                            //bootbox.dialog({title: 'Alert', message: response.message });
                            $scope.showAlertMessage('Alert',response.message);
                             $state.go("home", {});
                        });
                    }

            };
            
         }
    ]);
