'use strict';

/**
 * @ngdoc object
 * @name cart.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('cart')
        .controller('AddcartController', ['$scope', '$rootScope', 'RestCart', '$window', 'Constant', '$state', '$http', '$sce', '$filter',
            function($scope, $rootScope, RestCart, $window, Constant, $state, $http, $sce, $filter) {

                $rootScope.page_heading = "View Cart";
                //$rootScope.device_platform =  DEVICE_PLATFORM;    ;
                $scope.userObj = {};
                $scope.userObj.delivery = 2;
                
               // $scope.add_deliver_charges =0;
                $scope.grand_total =0;
               // $scope.proceed_Action =0;
                
                var session = angular.fromJson($window.localStorage.getItem("user_session"));

                $scope.customer_id = session.customer_id;
                $scope.token = session.token;
                $scope.subscription_id = session.subscription_id;
				
				
                
                $scope.ORDER_ACCEPT_CLOSE_AFTER = Constant.ORDER_ACCEPT_CLOSE_AFTER;  
                $scope.ORDER_ACCEPT_ALLOW_AFTER = Constant.ORDER_ACCEPT_ALLOW_AFTER;  
                
                $scope.restrict_after =parseInt($scope.ORDER_ACCEPT_CLOSE_AFTER) - parseInt(12);
                
		var today = new Date().getHours();
                
                if (today >= $scope.ORDER_ACCEPT_ALLOW_AFTER && today < $scope.ORDER_ACCEPT_CLOSE_AFTER) 
                {
                    $scope.is_display_makepayment = true;
                }
                else
                {
                    $scope.is_display_makepayment = false;
                }
				//$scope.cart_count =$rootScope.cart_count;	
				
				
				
                if($rootScope.order_product_cnt ==0  &&  $rootScope.sample_product_cnt > 0)
                {
                        $scope.is_min_am_required =0;   // Validation of Minimum Amount of 250 Not apply
                }
                else
                {
                        $scope.is_min_am_required =1;	// Validation For Minimum Amount of 250 Apply

                }
				
				
				
				/*
				if ($rootScope.total_amount < $scope.min_order_amount && $scope.subscription_id=='0' && $scope.is_min_am_required ==1)  // If Order Amount Less Than 250 & Is Not Subscriber then Restrict
				{
					//$scope.showAlertMessage('Alert',"To deliver order minimum amount should be Rs. "+$scope.min_order_amount+" /-."); 
					//$scope.add_deliver_charges = $scope.deliver_charges;
					//return false;
				}
				else
				{
					
					//$scope.add_deliver_charges =0;
				}
				*/
               /* Function to Place Order
                * onclik of Place Order Button
                */
                
                
                $scope.placeOrder = function() 
                {

                                        if (session == "" || session == null)
                                        {
                                                //alert("Please login to continue shopping.");
                                                //bootbox.dialog({title: 'Alert', message: "Please login to continue shopping."}); 
                                                $scope.showAlertMessage('Alert',"Please login to continue shopping.");
                                                $scope.showLoader(true);
                                                $state.go("login");

                                        }
                                        else
                                        {


                                                // Check Condition For Delivery Dates


                                                if ($scope.userObj.delivery == '1')   //  If Delivery Today & amount < 250
                                                {
                                                    
                                                    $scope.userObj.delivery = 1;  // Deliver Today
                                                    $scope.userObj.delivery_date = moment().format("YYYY-MM-DD");
                                                    
                                                        if ($rootScope.total_amount < $scope.min_order_amount && $scope.is_min_am_required ===1)
                                                        {
                                                                //alert("To deliver Today minimum cost should be Rs.250 /-.");
                                                                //bootbox.dialog({title: 'Alert', message: "To deliver Today minimum cost should be Rs.250 /-."}); 
                                                                //$scope.showAlertMessage('Info',"Delivery charges Rs. "+$scope.deliver_charges+"  /-  are added if minimum amount is less than Rs. "+$scope.min_order_amount+"  /-  "); 
                                                                //$scope.add_deliver_charges =$scope.deliver_charges ;
																
                                                                bootbox.confirm
                                                                ({
                                                                    message: "Delivery charges of Rs. "+$scope.deliver_charges+"  /-  are added if minimum amount is less than Rs. "+$scope.min_order_amount+"  /- ",
                                                                    buttons: {
                                                                            confirm: {
                                                                                    label: 'Procced',
                                                                                    className: 'btn-success'
                                                                            },
                                                                            cancel: {
                                                                                    label: 'Cancel',
                                                                                    className: 'btn-danger'
                                                                            }

                                                                    },
                                                                    closeButton:false,
                                                                    callback: function (result) {
                                                                        if(result==true)
                                                                        { 
                                                                            $scope.add_deliver_charges =$scope.deliver_charges;
                                                                                
                                                                            
                                                                            $scope.proceedtoPayment();
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                    }
                                                                });
																
                                                                //return false;
                                                        }
                                                        else
                                                        {
                                                                $scope.add_deliver_charges =0;
                                                                $scope.proceedtoPayment();
                                                                
                                                        }

                                                }
                                                else
                                                {
                                                    
                                                        $scope.userObj.delivery = 2;   // Deliver Tomorrow
                                                         $scope.tomorrow = new Date();
                                                         //$scope.userObj.delivery_date = $scope.tomorrow.getDate() + 1;
                                                         $scope.userObj.delivery_date = moment().add('days', 1).format("YYYY-MM-DD");
                                                         $scope.add_deliver_charges =0;
                                                    

                                                        if ($rootScope.total_amount < $scope.min_order_amount && $scope.subscription_id=='0' && $scope.is_min_am_required ==1)  // If Order Amount Less Than 250 & Is Not Subscriber then Restrict
                                                        {
                                                                //$scope.showAlertMessage('Info',"Delivery charges of Rs. "+$scope.deliver_charges+"  /-  are added if minimum amount is less than Rs. "+$scope.min_order_amount+"  /-  "); 
                                                                //$scope.add_deliver_charges = $scope.deliver_charges;
                                                                //return false;
                                                                //alert("2");
                                                                bootbox.confirm
                                                                ({
                                                                    message: "Delivery charges of Rs. "+$scope.deliver_charges+"  /-  will added if minimum amount is less than Rs. "+$scope.min_order_amount+"  /- ",
                                                                    buttons: {
                                                                            confirm: {
                                                                                    label: 'Procced',
                                                                                    className: 'btn-success'
                                                                            },
                                                                            cancel: {
                                                                                    label: 'Cancel',
                                                                                    className: 'btn-danger'
                                                                            }

                                                                    },
                                                                    closeButton:false,
                                                                    callback: function (result) {
                                                                        
                                                                        if(result==true)
                                                                        { 
                                                                           
                                                                            $scope.add_deliver_charges =$scope.deliver_charges;
                                                                                
                                                                            
                                                                            $scope.proceedtoPayment();
                                                                        }
                                                                        else
                                                                        {
                                                                            //alert("4");
                                                                            
                                                                        }
                                                                        
                                                                    }
                                                                });
                                                                
                                                                
                                                        }
                                                        else
                                                        {
                                                             $scope.add_deliver_charges =0;
                                                            $scope.proceedtoPayment();
                                                                 
                                                        }
                                                }

                                                // alert($scope.userObj.delivery_date);
                                                
                                        }


                                };
                                
                            /* Set Delivery Day Values
                             * 
                             */    
                            $scope.setDeliveryDay = function(delivery_day,total_amount)
                            {
                                //alert($scope.min_order_amount +"<>"+total_amount);
//                                if(delivery_day=='1' &&  total_amount < $scope.min_order_amount)
//                                {
//                                  $scope.add_deliver_charges = $scope.deliver_charges;      
//                                }
//                                else if(delivery_day=='2' &&  total_amount < $scope.min_order_amount && $scope.subscription_id==0)
//                                {
//
//                                    $scope.add_deliver_charges = $scope.deliver_charges;  
//                                }
//                                else
//                                {
//                                    $scope.add_deliver_charges = 0;
//                                }
                            }        
                            
                            
                            $scope.proceedtoPayment = function()
                            {
                                
                            
                               
                                $scope.showLoader(false);
                                $scope.cart_object = {'customer_id': $scope.customer_id, 'token': $scope.token, 'total_amount': $rootScope.total_amount, 'product_list': $rootScope.products_cart, 'delivery_date': $scope.userObj.delivery_date, 'delivery_charges':$scope.add_deliver_charges}
                                
                               

                                RestCart.placeProductOrder($scope.cart_object).success(function(response)
                                {
                                        $scope.status = response["status"];
                                        $rootScope.products_cart = [];
                                        $rootScope.cart_count = 0;

                                        if ($scope.status == "success")
                                        {

                                                $rootScope.cart_count = 0;
                                                $scope.invoice_id =response["result"]["invoice_id"];
                                                $scope.final_amount_paid =response["result"]["final_amount_paid"];


                                                $scope.product_count = 0;
                                                var tid = new Date().getTime();

                                                var ccavenue_url = Constant.CCAVENUE_URL + "/ccavRequestHandler.php?invoice_id=" + response["result"]["invoice_id"] + "&customer_id=" + $scope.customer_id + "&invoice_amount=" + $scope.final_amount_paid + "&tid=" + tid;

                                                // $state.go("thank_you");
                                                // $scope.showLoader(true);
                                                 //alert(ccavenue_url );

                                                if (Constant.DEVICE_PLATFORM === "web")
                                                {
                                                        $window.open(ccavenue_url);
                                                        $scope.showLoader(false);

                                                        setTimeout(function() {
                                                                //alert( $scope.invoice_id);
                                                                $state.go("thank_you", { 'invoice_id': $scope.invoice_id});
                                                        }, 6500);
                                                }
                                                else if (Constant.DEVICE_PLATFORM === "Android")
                                                {
                                                        //var options = {location: 'yes', EnableViewPortScale: 'yes', toolbar: 'yes'}; 
                                                        var ref = cordova.InAppBrowser.open(ccavenue_url, '_blank', 'location=no,EnableViewPortScale=yes,toolbar=no');
                                                        $scope.showLoader(true);

                                                        // attach listener to loadstart
                                                        ref.addEventListener('loadstart', function(event) {
                                                                var urlSuccessPage = Constant.ORDER_RESPONSE_HANDLER_URL;
                                                                if (event.url == urlSuccessPage) {

                                                                                setTimeout(function() {
                                                                                   ref.close();
                                                                                   $state.go("thank_you", { 'invoice_id': $scope.invoice_id});
                                                                                }, 12000);


                                                                        //$state.go("thank_you", { 'invoice_id': $scope.invoice_id});
                                                                }
                                                        });
                                                }

                                        }
                                        else
                                        {
                                                $scope.showAlertMessage("Alert",$scope.message); 
                                                if ($scope.status === 'invalid_token') {
                                                        $rootScope.invalidToken();
                                                }

                                        }

                                }).error(function(response) {
                                        //alert("Error in connection.");
                                        //bootbox.dialog({title: 'Alert', message: "Error in connection."});
                                        $scope.showAlertMessage('Alert',"Error in connection");    

                                        $scope.showLoader(true);
                                });

                                
                            }               
                                




                }
        ])

        .controller('ThankYouController', ['$scope', '$rootScope', 'RestCart', '$window', 'Constant', '$state',
            function($scope, $rootScope, RestCart, $window, Constant, $state) {

                $rootScope.page_heading = "Thank You";
                $rootScope.products_cart = [];
                $rootScope.cart_count = 0;
                
                $scope.invoice_id = $state.params.invoice_id;
                var session = angular.fromJson($window.localStorage.getItem("user_session"));

                $scope.customer_id = session.customer_id;
                $scope.token = session.token;
                
                $scope.showLoader(false);
                
                $scope.cart_object = {'customer_id': $scope.customer_id, 'token': $scope.token,'invoice_id': $scope.invoice_id}
                $rootScope.products_cart = [];
				
				$rootScope.sample_product_cnt = 0;
				$rootScope.order_product_cnt = 0
				
                // Check Order Status 
                RestCart.checkOrderPaymentStatus($scope.cart_object).success(function(response)
                {                
                    $scope.status = response["status"];
                    
                    if($scope.status == "success")
                    {
                        
                        $scope.showLoader(true);
                        $scope.payment_status = response["result"]["payment_status"];
                       // alert($scope.payment_status);
                        $scope.message_display = response["result"]["message_display"];
                    }
                    else
                    {
                        $scope.showAlertMessage("Alert",$scope.message); 
                        if ($scope.status === 'invalid_token') {
                            $rootScope.invalidToken();
                        }
                        
                    }

                }).error(function(response) {                   
                    //alert("Error in connection.");
                     //bootbox.dialog({title: 'Alert', message: "Error in connection."}); 
                     $scope.showAlertMessage('Alert',"Error in connection.");   
                    $scope.showLoader(true);	
                });




            }
        ]);
