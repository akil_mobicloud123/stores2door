'use strict';

/**
 * @ngdoc object
 * @name cart.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('cart')
    .controller('MakePaymentController', ['$scope','$rootScope','RestCart','$window','Constant','$state','$http','$sce','$stateParams',
        function($scope,$rootScope,RestCart,$window,Constant,$state,$http,$sce,$stateParams) {
            
            $rootScope.page_heading = "Make Payment";
            //$rootScope.device_platform =  DEVICE_PLATFORM;    ;
              $scope.invoice_id = $stateParams.invoice_id;
                
            $scope.getpaymentscreen = function(invoice_id) {

                $scope.showLoader(false);
                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                $scope.customer_id = session.customer_id;
                $scope.token = session.token;                  
                
                $scope.cart_object = {'customer_id':$scope.customer_id,'token':$scope.token,'invoice_id':invoice_id}
                
                RestCart.getPaymentScreen($scope.cart_object).success(function(response)
                {                
                            $scope.status = response["status"];
                            $rootScope.products_cart = {};
                            $rootScope.cart_count =0;    
                            
                            $scope.myText = ""; // ng-model for html response container in view

                            $scope.ProfileData = {
                                 //custom and required data fields
                            };
                            $scope.html = '<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"  ><input type="text" name="access_code" id="access_code" value="'+ response["result"]["accessCode"].trim() +'" style="display:none;" ><input type="text" id="encRequest" name="encRequest" value="'+ response["result"]["encRequest"].trim() +'" style="display:none;" ><script language="javascript">document.redirect.submit();</script></form>';


                            $scope.trustedHtml = $sce.trustAsHtml($scope.html);

                         //  $state.go("thank_you", {}); 
                           $scope.showLoader(true);
                  
                  

                }).error(function(response) {                   
                    //alert("Error in connection.");
                     //bootbox.dialog({title: 'Alert', message: "Error in connection."});
                     $scope.showAlertMessage('Alert',"Error in connection."); 
                    $scope.showLoader(true);	
                });

            };
            
            $scope.getpaymentscreen($scope.invoice_id);
        
        }
    ]);
	