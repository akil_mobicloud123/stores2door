'use strict';

/**
 * @ngdoc service
 * @name products.Services.Rest
 * @description Rest Factory
 */
    angular
        .module('products')
        .factory('RestCart', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    
                    placeProductOrder: function (post_data) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'placeProductOrder';
                        
                        var config = {};
                        var rest_data = post_data;
                        return $http.post(url, rest_data,config);
                    },
                    getPaymentScreen: function (post_data) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getPaymentScreen';
                        
                        var config = {};
                        var rest_data = post_data;
                        return $http.post(url, rest_data,config);
                    },
                    checkOrderPaymentStatus: function (post_data) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'checkOrderPaymentStatus';
                        
                        var config = {};
                        var rest_data = post_data;
                        return $http.post(url, rest_data,config);
                    },
                    
                    
                }
        }]);