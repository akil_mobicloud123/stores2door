'use strict';

/**
 * @ngdoc object
 * @name cart.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the cart module
 */
angular
        .module('cart')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name cart.config.route
                 * @eventOf cart.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                
                        .state('addcart', {
                            url: '/addcart',
                            templateUrl: 'modules/cart/views/addcart.html',
                            controller: 'AddcartController'
                        })

                        .state('make_payment', {
                            url: '/make_payment/:invoice_id?',
                            templateUrl: 'modules/cart/views/make_payment.html',
                            controller: 'MakePaymentController'
                        })

                        .state('thank_you', {
                            url: '/thank_you/:invoice_id?',
                            templateUrl: 'modules/cart/views/thank_you.html',
                            controller: 'ThankYouController'
                        })

                        .state('payment_status', {
                            url: '/payment_status/:invoice_id?',
                            templateUrl: 'modules/billing/views/payment_status.html',
                            controller: 'PaymentStatusController'
                        })

                        .state('delevery_type', {
                            url: '/delevery_type',
                            templateUrl: 'modules/cart/views/delevery_type.html',
                            controller: 'DeleveryTypeController'
                        }) 

                        .state('shiping-info', {
                            url: '/shiping-info',
                            templateUrl: 'modules/cart/views/shiping-info.html',
                            controller: 'ShipingInfoController'
                        }) 


                        .state('order-summary', {
                            url: '/order-summary',
                            templateUrl: 'modules/cart/views/order-summary.html',
                            controller: 'OrderSummaryController'
                        }) 


                        .state('date-time', {
                            url: '/date-time',
                            templateUrl: 'modules/cart/views/date-time.html',
                            controller: 'DateTimeController'
                        }) 


            }
        ]);
