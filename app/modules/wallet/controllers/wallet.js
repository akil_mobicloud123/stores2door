'use strict';

/**
 * @ngdoc object
 * @name wallet.Controllers.HomeController
 * @description wallet/WalletController
 * @requires ng.$scope
 */
angular
    .module('wallet')
    .controller('WalletController', ['$scope','$rootScope','$window','RestWallet','$state',
        function($scope,$rootScope,$window,RestWallet,$state) {
            
          $rootScope.page_heading = "Wallet Log";  
          

        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
        $scope.customer_id =   $scope.session.customer_id;
        $scope.token =   $scope.session.token;
        $scope.totalCredit = 0;
        $scope.totalDebit = 0;
        $scope.current_balance =0;  
        
        /* Get Wallet Log Details
         * Date : 25/11/2017
         * 
         */



        $scope.getWalletTransactionLog = function(customer_id,token) {

                $scope.showLoader(false);

                RestWallet.getWalletTransactionLog(customer_id,token).success(function(response)
                {

                        if (response["status"] == "success")
                        {

                                $scope.totalCount = response["totalCount"];
                                $scope.wallet_log = response["result"];
                                $scope.showLoader(true);
                                $scope.no_list = false; 
                                
                                $scope.totalCredit = response["totalCredit"];
                                $scope.totalDebit = response["totalDebit"];
 
                                $scope.current_balance =$scope.totalCredit - $scope.totalDebit;
                        }
                        else
                        {
                           // alert(response["message"]);
                                $scope.no_list = true; 
                                $scope.showLoader(true);
                                $scope.current_balance =0;
                                $scope.totalCredit = 0;
                                $scope.totalDebit = 0;
                                
                               // $scope.showAlertMessage("Alert",response["message"]); 
                                if (response["status"] === 'invalid_token') {
                                    $scope.showAlertMessage("Alert",response["message"]); 
                                    $rootScope.invalidToken();
                                }

                        }

                }).error(function(response) {
                            //alert("Error in connection.");
                            $scope.showLoader(true);	
                            //alert(response.message);   
                            //bootbox.dialog({title: 'Alert', message: response.message }); 
                            //$scope.showAlertMessage('Alert',response.message);
                            $state.go("home", {});
                });

        };

        $scope.getWalletTransactionLog($scope.customer_id,$scope.token);


		 
          
        }
    ]);


	
  
	
   
  