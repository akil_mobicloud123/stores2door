'use strict';

/**
 * @ngdoc object
 * @name wallet.Controllers.AdvancePaymentController
 * @description wallet/AdvancePaymentController
 * @requires ng.$scope
 */
angular
    .module('wallet')
    .controller('AdvancePaymentController', ['$scope','$rootScope','$window','RestAdvancePayment','$state','Constant',
        function($scope,$rootScope,$window,RestAdvancePayment,$state,Constant) {
            
          $rootScope.page_heading = "Advance Payment";  
          

        $scope.session = angular.fromJson($window.localStorage.getItem("user_session"));
         
        //console.log($scope.session);
        $scope.customer_id =   $scope.session.customer_id;
        $scope.token =   $scope.session.token;
        
        /* Get Advance Payment Details
         * Date : 25/11/2017
         * 
         */

        $scope.getAdvancePaymentAmount = function(customer_id,token) {

                $scope.showLoader(false);

                RestAdvancePayment.getAdvancePaymentAmount(customer_id,token).success(function(response)
                {

                        if (response["status"] == "success")
                        {

                                $scope.advanceAmount = response["advanceAmount"];
				$scope.available_wallet_amount = response["available_wallet_amount"];
								
                                if( parseInt($scope.available_wallet_amount) >= parseInt($scope.advanceAmount))
                                {
                                        $scope.advance_final_amount = 0;

                                }
                                else
                                {
                                        $scope.advance_final_amount =   $scope.advanceAmount- $scope.available_wallet_amount;
                                }
								
                                $scope.showLoader(true);
                                $scope.no_list = false; 
                                
                        }
                        else
                        {
                           // alert(response["message"]);
                                $scope.no_list = true; 
                                $scope.showLoader(true);
                                $scope.advanceAmount =0;
                                
                               // $scope.showAlertMessage("Alert",response["message"]); 
                                if (response["status"] === 'invalid_token') {
                                    $scope.showAlertMessage("Alert",response["message"]); 
                                    $rootScope.invalidToken();
                                }

                        }

                }).error(function(response) {
                            //alert("Error in connection.");
                            $scope.showLoader(true);	
                            //alert(response.message);   
                            //bootbox.dialog({title: 'Alert', message: response.message }); 
                            //$scope.showAlertMessage('Alert',response.message);
                            $state.go("home", {});
                });

        };
        
        
        /* 
         * Make Advance Payment
         * fun :
         * Date : 27/04/2018
         * 
         */


        $scope.makeAdvancePayment = function(customer_id, advance_amount,wallet_amount)
        {


            var session = angular.fromJson($window.localStorage.getItem("user_session"));


            if (session == "" || session == null)
            {
                //alert("Please login to make invoice Payment.");
                //bootbox.dialog({title: 'Alert', message: "Please login to make invoice Payment." }); 
                $scope.showAlertMessage('Alert', "Please login to make Advance Payment.");
                $scope.showLoader(true);
                $state.go("login");

            }
            else
            {

                $scope.customer_id = session.customer_id;
                $scope.token = session.token;

                $scope.advance_amount = advance_amount;
				
				$scope.wallet_amount = wallet_amount;

                var tid = new Date().getTime();

                var ccavenue_url = Constant.CCAVENUE_URL + "/ccavAdvancePaymentRequestHandler.php?customer_id=" + $scope.customer_id + "&advance_amount=" + $scope.advance_amount +"&wallet_amount_deduct=" + $scope.wallet_amount + "&tid=" + tid;


                if (Constant.DEVICE_PLATFORM === "web")
                {
                    $window.open(ccavenue_url);
                    $scope.showLoader(false);
                    setTimeout(function() {
                        $state.go("advance_payment_status");
                    }, 6500);
                }
                else if (Constant.DEVICE_PLATFORM === "Android")
                {
                    // var options = {location: 'yes', EnableViewPortScale: 'yes', toolbar: 'yes'}; 

                    var ref = cordova.InAppBrowser.open(ccavenue_url, '_blank', 'location=no,EnableViewPortScale=yes,toolbar=no');
                    $scope.showLoader(true);

                    // attach listener to loadstart
                    ref.addEventListener('loadstart', function(event) {
                        var urlSuccessPage = Constant.ADVANCE_PAYMENT_CCAVENUE_RESPONSE_URL;
                       // alert("Alt:" + event.url +"<=="+ urlSuccessPage);
                        
                        if (event.url == urlSuccessPage) {
                           // alert("IN");
                            setTimeout(function() {
                                ref.close();
                                $state.go("wallet");
                            }, 10000);
                        }
                        else
                        {
                            //alert("OUT");
                        }
                    });
                }

            }


        };

        
        

        $scope.getAdvancePaymentAmount($scope.customer_id,$scope.token);


		 
          
        }
    ])
    
    .controller('AdvancePaymentStatusController', ['$scope', '$rootScope', 'RestCart', '$window', 'Constant', '$state',
        function($scope, $rootScope, RestCart, $window, Constant, $state) {

            $rootScope.page_heading = "Advance Payment Status";
            $rootScope.cart_count = 0;

            $scope.payment_status = $state.params.payment_status;
            var session = angular.fromJson($window.localStorage.getItem("user_session"));

            $scope.customer_id = session.customer_id;
            $scope.token = session.token;

            $scope.showLoader(false);



        }
    ]);
    


	
  
	
   
  
