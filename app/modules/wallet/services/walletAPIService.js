'use strict';

/**
 * @ngdoc service
 * @name wallet.Services.Rest
 * @description Rest Factory
 */
    angular
        .module('wallet')
        .factory('RestWallet', ['$http','Constant', function ($http,Constant) {                
                var urlBase = Constant.urlBase;      
                return{
                    

                    getWalletTransactionLog: function (customer_id,token) {
                        // Set the Content-Type 
                        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                        // Delete the Requested With Header
                        delete $http.defaults.headers.common['X-Requested-With'];
                        
                        var url = urlBase + 'getWalletTransactionLog';
                        
                        var config = {};
                        var rest_data = {"customer_id":customer_id,"token":token};
                        return $http.post(url, rest_data,config);
                    },
					
                    
                }
        }]);