'use strict';

/**
 * @ngdoc object
 * @name billing.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the billing module
 */
angular
        .module('wallet')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name wallet.config.route
                 * @eventOf wallet.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider
                        
                        .state('wallet', {
                           url: '/wallet',
                           templateUrl: 'modules/wallet/views/wallet_log.html',
                           controller: 'WalletController'
                       })

                        .state('advance_payment', {
                           url: '/advance_payment',
                           templateUrl: 'modules/wallet/views/advance_payment.html',
                           controller: 'AdvancePaymentController'
                       })

                        .state('advance_payment_status', {
                           url: '/advance_payment_status',
                           templateUrl: 'modules/wallet/views/advance_payment_status.html',
                           controller: 'AdvancePaymentStatusController'
                       })
                
                       

            }
        ]);
