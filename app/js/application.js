'use strict';

angular
    .module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular
    .module(ApplicationConfiguration.applicationModuleName)
    .config(['$locationProvider', 'snapRemoteProvider',
           function($locationProvider, snapRemoteProvider) {
               snapRemoteProvider.globalOptions.disable = 'right';
               $locationProvider.hashPrefix('!');
           }
       ]).run(function ($rootScope, $window,$state,$timeout,Constant,$stateParams,restAPICore ) {

       $rootScope.ph_numbr = /^[2-9]\d{9}$/; // mobile no validation		
       // for Cart
        $rootScope.cart_count = 0 ;
        $rootScope.products_cart = [];   
        $rootScope.subscription_count = 0 ;
        $rootScope.products_subscription = [];   
        $rootScope.product_subscription_count = 0 ;
        $rootScope.APP_Version = Constant.APP_VERSION;  
        //$rootScope.min_order_amount = 250;
        //$rootScope.deliver_charges = Constant.Delivery_charge;
        //$rootScope.DEVICE_PLATFORM = Constant.DEVICE_PLATFORM;

        var session = angular.fromJson($window.localStorage.getItem("user_session"));

        if(!isEmpty(session)) 
        {
            $rootScope.min_order_amount = parseInt(session.minimum_order_amount);
            $rootScope.deliver_charges = parseInt(session.delivery_charges);           
        }    

        $rootScope.sample_product_cnt =0;
        $rootScope.order_product_cnt = 0;
        var lastTimeBackPress=0;
        var timePeriodToExit=3500;
		
       
       
            function onDeviceReady() 
            {
                document.addEventListener("backbutton", function(e){


                    if (window.location.hash == '#!/') 
                    {

                            if(new Date().getTime() - lastTimeBackPress < timePeriodToExit)
                            {
                                // Check if user is Login
//                                if (!$window.sessionStorage.token) 
//                                {
//                                    navigator.app.exitApp();
//                                }
//                                else
//                                {
//                                     $rootScope.logout()
//                                }

                                  //exitAppPopup();
                                  navigator.app.exitApp();
                            }
                            else
                            {
                                 window.plugins.toast.showWithOptions(
                                     {
                                       message: "Press again to exit app.",
                                       duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
                                       position: "bottom",
                                       addPixelsY: -120  // added a negative value to move it up a bit (default 0)
                                     }
                                   );

                                 lastTimeBackPress=new Date().getTime();
                            }                    



                    }else{
                        history.back();
                    };
                });        


            }
            
            
            function ConfirmExit(stat){
                //alert("Inside ConfirmExit");
                if(stat == "1"){
                    //navigator.app.exitApp();
                    $rootScope.logout()
                }else{
                    return;
                };
            };
            
            // Handle Back Button Key 
            function onBackKeyDown(e) 
            {
               if($rootScope.currentState=="thank_you")
               {
                e.preventDefault();
               }
            }
       
        // Check If Session Value Exist
        $rootScope.checkIsValidToken = function () 
        {

            var session = angular.fromJson($window.localStorage.getItem("user_session"));

            if(!isEmpty(session)) 
            {
                $rootScope.customer_id = session.customer_id;
                $rootScope.token = session.token;           


                $rootScope.cust_info = {"customer_id":$rootScope.customer_id,"token":$rootScope.token};

                restAPICore.getCustomerDetail($rootScope.cust_info).success(function(response) {

                    if (response["status"] == "success")
                    {
                        // Do Nothing
                       // alert("IN Log2");
                    }
                    else
                    {
                        //alert("IN Log");
                        delete $window.localStorage.user_session; 
                        $window.localStorage.removeItem("user_session");
                         // Check the session exist or not
                        var session = angular.fromJson($window.localStorage.getItem("user_session"));
                        $rootScope.is_session = !isEmpty(session);

                    }


                }).error(function(response) {
                    //alert(response.message);                                
                    $state.go("home", {});
                });          

            }    
        };
    
       
       // Check if Token Exist For Login User
       $rootScope.checkIsValidToken();
        
//        if(navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) 
//        {
//            $rootScope.DEVICE_PLATFORM = 'Android';
//        } 
//        else   // For Web Only
//        {
//            $rootScope.DEVICE_PLATFORM ="web";
//        }


       /**
         * Add product to cart
         * @param {type} id
         * @param {type} price
         * @param {type} quantity
         * @param {type} image
         * @param {type} product_weight
         * @returns {undefined}
         */
        $rootScope.addProducts = function(id,title,price,quantity,image,product_weight,sample_product,order_product){
            debugger
			$rootScope.sample_product_cnt = $rootScope.sample_product_cnt + sample_product;
			$rootScope.order_product_cnt = $rootScope.order_product_cnt + order_product;
			
			//alert($rootScope.sample_product_cnt + "<>" +$rootScope.order_product_cnt );
			
            var total = parseInt(price)*quantity;
            //$rootScope.removeProducts(id);     
            if(hasElement('product_id',id,$rootScope.products_cart) > -1 )
            {
                var index = hasElement('product_id',id,$rootScope.products_cart);
                $rootScope.products_cart[index].product_quantity = parseInt($rootScope.products_cart[index].product_quantity) + 1 ; 
                $rootScope.products_cart[index].product_total = parseInt($rootScope.products_cart[index].product_quantity) * parseInt(price) ;  
            }else{
               $rootScope.products_cart.push({product_id:id, product_title:title,product_rate:price, product_quantity:1, product_image:image, product_weight:product_weight,product_total:price});                              
            }
            $rootScope.cart_count = Object.keys($rootScope.products_cart).length;
            $rootScope.cart_amount = $rootScope.products_cart.sum("product_total");    
			
            var total_amount  = parseInt($rootScope.cart_amount) ;
            $rootScope.total_amount = total_amount;  
            
           /*
            if(total_amount < $rootScope.min_order_amount)
            {
                $rootScope.add_deliver_charges = $rootScope.deliver_charges;       
            }
            else
            {
                $rootScope.add_deliver_charges = 0;       
            }
           */ 
           // alert($rootScope.add_deliver_charges);
                  
        }
        /**
         * Remove product from cart
         * @param {type} product_id
         * @returns {undefined}
         */
        $rootScope.removeProducts = function(product_id){    
            
            if($rootScope.GetdataIfhasElement('product_id',product_id,$rootScope.products_cart).product_quantity > 1)
            {
                var index = hasElement('product_id',product_id,$rootScope.products_cart);
                $rootScope.products_cart[index].product_quantity = parseInt($rootScope.products_cart[index].product_quantity) - 1 ; 
                $rootScope.products_cart[index].product_total = parseInt( $rootScope.products_cart[index].product_quantity * $rootScope.products_cart[index].product_rate );                
            }else{
                $rootScope.products_cart = $.grep($rootScope.products_cart, function(element, index){return element.product_id == product_id}, true);          
                $rootScope.cart_count = Object.keys($rootScope.products_cart).length;                
            }            
            $rootScope.cart_amount = $rootScope.products_cart.sum("product_total");     
            var total_amount  = parseInt($rootScope.cart_amount) ;
            
            $rootScope.total_amount = total_amount;  
            //$rootScope.delivery_charge = Constant.Delivery_charge; 
            
           /* if(total_amount < $rootScope.min_order_amount)
            {
                $rootScope.add_deliver_charges = $rootScope.deliver_charges;       
            }
            else
            {
                $rootScope.add_deliver_charges = 0;       
            }
		  */	
            
        }
        
        function hasElement(prop, value, data) {
            var result = -1;
            if(!isEmpty(data))
            {
                data.forEach(function(obj, index) {
                    if (prop in obj && obj[prop] === value) {
                        result = index;
                        return false;
                    }
                });
            }
            return result;
        }
        
        $rootScope.GetdataIfhasElement = function(prop, value, data) {
            var result = -1;
            if(!isEmpty(data))
            {
                data.forEach(function(obj, index) {
                    if (prop in obj && obj[prop] === value) {                      
                        result = obj;
                        return false;                        
                    }
                });
            }
            return result;
        }
        
        $rootScope.CheckhasElement = function(prop, value, data){ 
        var result = -1;
        if(!isEmpty(data))
        {
            data.forEach(function(obj, index) {
            if (prop in obj && obj[prop] === value) {
                result = index;
                return false;
            }

            });
        }
        return result;
        }
        
        
        $rootScope.CheckhasDualElement = function(prop,prop1, value,value1, data){ 
        var result = -1;
        if(!isEmpty(data))
        {
            data.forEach(function(obj, index) {
            if (prop in obj && obj[prop] === value && obj[prop1] === value1) {
                result = index;
                return false;
            }

            });
        }
        return result;
        }
        
        /**
         * Add product subscription array
         * @param {type} id
         * @param {type} price
         * @param {type} quantity
         * @param {type} image
         * @param {type} product_weight
         * @returns {undefined}
         */
         $rootScope.addsubProducts = function($event,id,price,quantity,image,product_weight,milk_type,subscription_pattern){
            var total = price*quantity;
            
            //$rootScope.removeProducts(id);    
            var index_product = hasElement('product_id',id,$rootScope.products_subscription);
            $rootScope.product_subscription_count = index_product;
            
            if(hasElement('product_id',id,$rootScope.products_subscription) > -1 )
            {               
                $rootScope.products_subscription[index_product].product_quantity = parseInt($rootScope.products_subscription[index_product].product_quantity) + 1 ; 
                $rootScope.products_subscription[index_product].product_total = parseInt( $rootScope.products_subscription[index_product].product_quantity * price );  
            }else{
               $rootScope.products_subscription.push({product_id:id, product_rate:price, product_quantity:1, product_image:image, product_weight:product_weight,product_total:price,milk_type:milk_type,subscription_pattern:subscription_pattern});  
            }     
            
            $rootScope.subscription_count = Object.keys($rootScope.products_subscription).length;
            $rootScope.sub_amount = $rootScope.products_subscription.sum("product_total");    
            var total_amount  = parseInt($rootScope.sub_amount);
            $rootScope.sub_total_amount = total_amount;               
            
            // add active class and chnage quantity
            angular.element($event.currentTarget).closest('.subscription-item').find('.subs-days').find('.day_quantity').html('0');
            
            if(!angular.element($event.currentTarget).closest('.subscription-item').find('input:radio').is(':checked')) // check nearest radio buttone selected ot not
            {
                // select the daily as selected on quantity change
                angular.element($event.currentTarget).closest('.subscription-item').find('.Sun,.Mon,.Tue,.Wed,.Thu,.Fri,.Sat').parent().addClass("active_day");
                angular.element($event.currentTarget).closest('.subscription-item').find('.daily').prop('checked', true);
            }
            if(index_product == -1 )
            {
                angular.element($event.currentTarget).closest('.subscription-item').find('.active_day').find('.day_quantity').html('1');
            }else{
                angular.element($event.currentTarget).closest('.subscription-item').find('.active_day').find('.day_quantity').html($rootScope.products_subscription[index_product].product_quantity);
            }
        }
        
        /**
         * Remove product subscription array
         * @param {type} product_idyz
         * @returns {undefined}
         */
        $rootScope.removesubProducts = function($event,product_id){   
            var index_product = hasElement('product_id',product_id,$rootScope.products_subscription);
            $rootScope.product_subscription_count = index_product;
            if($rootScope.GetdataIfhasElement('product_id',product_id,$rootScope.products_subscription).product_quantity > 1)
            {                
                $rootScope.products_subscription[index_product].product_quantity = parseInt($rootScope.products_subscription[index_product].product_quantity) - 1 ; 
                $rootScope.products_subscription[index_product].product_total = parseInt( $rootScope.products_subscription[index_product].product_quantity * $rootScope.products_subscription[index_product].product_rate );                
            }else{
                $rootScope.products_subscription = $.grep($rootScope.products_subscription, function(element, index_product){return element.product_id == product_id}, true);          
                $rootScope.subscription_count = Object.keys($rootScope.products_subscription).length;
                }
            $rootScope.sub_amount = $rootScope.products_subscription.sum("product_total");     
            var total_amount  = parseInt($rootScope.sub_amount);
            $rootScope.sub_total_amount = total_amount;  
            
            // add active class and chnage quantity
            angular.element($event.currentTarget).closest('.subscription-item').find('.subs-days').find('.day_quantity').html('0');
            if(index_product != -1) {
                angular.element($event.currentTarget).closest('.subscription-item').find('.active_day').find('.day_quantity').html($rootScope.products_subscription[index_product].product_quantity);
            }else{
                angular.element($event.currentTarget).closest('.subscription-item').find('.subs-days').find('.day_quantity').html('0');
            }
           
            
        }
        
        Array.prototype.sum = function (prop) {
            var total = 0
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total = parseInt(total) + parseInt(this[i][prop]);
            }
            return total
        }

                
       // $rootScope.loading = true;
        
        //*****************************************/ 
        // Function : showLoader
        // Parameter : flag
        // Desc : Handle the Show / Hide of Loader

        $rootScope.showLoader = function ($flag) {
           
            $rootScope.loaderShow = $flag;
        };

        //*****************************************/ 
        // Function : LogOut User
        // Parameter : flag
        // Desc : Logout User

        $rootScope.logout = function () {
            $rootScope.loading = true;
            delete $window.localStorage.user_session; 
            $window.localStorage.removeItem("user_session");
             // Check the session exist or not
            var session = angular.fromJson($window.localStorage.getItem("user_session"));
            $rootScope.is_session = !isEmpty(session);
            $state.go('home', {});
        };
        
        
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.showLoader(false);
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams,$location, $anchorScroll,$stateParams) {
           
           $rootScope.showLoader(true);
            
            // Check the session exist or not
            var session = angular.fromJson($window.localStorage.getItem("user_session"));
            
            $rootScope.is_session = !isEmpty(session);
            
            $rootScope.currentState = toState.name;
            
            
            if(!isEmpty(session)) {
                $rootScope.subscription_id = session.subscription_id;
                $rootScope.customer_name = session.customer_name;
            }
            else
            {
                if($rootScope.currentState=="home" && typeof $rootScope.is_page_history != "undefined")
                {
                   // window.location.reload(true);
                }               
                else if($rootScope.currentState=="login" || $rootScope.currentState=="registration" || $rootScope.currentState=="otp" || $rootScope.currentState=="enterOtp" || $rootScope.currentState=="termsofuse" || $rootScope.currentState=="howitworks" || $rootScope.currentState=="customercare" || $rootScope.currentState=="products" || $rootScope.currentState=="addcart" )
                {
                    // do nothings
                }
                else
                {
                    $state.go('home', {});
                }
                $rootScope.is_page_history = $rootScope.currentState;
                
            }
			
            /****** Scroll Top *********/
            //e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 1000);
            /**** Scroll Top End ******/
            

        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) {
            //hide loading gif
            $rootScope.showLoader(true);
        });
       
        function isEmpty(myObject) {
            for (var key in myObject) {
                if (myObject.hasOwnProperty(key)) {
                    return false;
                }
            }

            return true;
        }

       
        //*****************************************/ 
        // Function : Show Alert Message Box
        // Parameter : flag
        // Desc : Bootbox Alert Box

        $rootScope.showAlertMessage = function (title,message) {
           
            //bootbox.alert({title: 'Welcome', message: response.message });
            if(message =="" || message ==null)
            {
            }
            else
            {
               // bootbox.dialog({title: title, message: message, });
                bootbox.dialog({title: title, message: message,buttons: {ok: {
                label: "OK",
                className: 'btn-info',
                callback: function(){
                   // Example.show('Custom OK clicked');
                }
                 }
            } });
                
            }
        };
       

        $rootScope.invalidToken = function () {
            delete $window.localStorage.user_session; 
            $window.localStorage.removeItem("user_session");
             // Check the session exist or not
         
            
            $state.go('home', {});
        };
       
        $rootScope.time_lock_status = function(t1, t2) 
        {
            var today = new Date().getHours();

            if(today >= Constant.ORDER_ACCEPT_ALLOW_AFTER && today < Constant.ORDER_ACCEPT_CLOSE_AFTER) 
            {
                return 1;
                
               
            }
            else
            {
                
                    // If Time is Exceed, then check if Order is for 3rd Day 


                    var one_day = 1000 * 60 * 60 * 24;

                    var x = t1.split("-");
                    var y = t2.split("-");

                    var date1 = new Date(x[0], (x[1] - 1), x[2]);
                    var date2 = new Date(y[0], (y[1] - 1), y[2]);

                    var month1 = x[1] - 1;
                    var month2 = y[1] - 1;

                    var Diff = Math.ceil((date2.getTime() - date1.getTime()) / (one_day));

                    var date_diff = Diff * -1;

                    if(date_diff > 1)
                    {
                     return 1;    
                    }
                    else
                    {
                       return 0; 
                    }
               
                
                
            }
            
            
            
        };
       
       
       
        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/))
        {
            document.addEventListener("deviceready", onDeviceReady, false);
        }
        else   // For Web Only
        {
            onDeviceReady();
        }
        
        
        
        
           
    });

//Then define the init function for starting up the application
angular
    .element(document)
    .ready(function() {
        if (window.location.hash === '#_=_') {
            window.location.hash = '#!';
        }
        angular
            .bootstrap(document,
                [ApplicationConfiguration.applicationModuleName]);
    });
